# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ConfigureSlotsDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_ConfigureSlotsDialog(object):
    def setupUi(self, ConfigureSlotsDialog):
        if not ConfigureSlotsDialog.objectName():
            ConfigureSlotsDialog.setObjectName(u"ConfigureSlotsDialog")
        ConfigureSlotsDialog.resize(666, 549)
        ConfigureSlotsDialog.setSizeGripEnabled(True)
        self.gridLayoutWidget = QWidget(ConfigureSlotsDialog)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(15, 20, 641, 521))
        self.configureSlotsGrid = QGridLayout(self.gridLayoutWidget)
        self.configureSlotsGrid.setObjectName(u"configureSlotsGrid")
        self.configureSlotsGrid.setContentsMargins(0, 0, 0, 0)
        self.lvPowerSupplyLabel = QLabel(self.gridLayoutWidget)
        self.lvPowerSupplyLabel.setObjectName(u"lvPowerSupplyLabel")

        self.configureSlotsGrid.addWidget(self.lvPowerSupplyLabel, 0, 1, 1, 1)

        self.arduinoLabel = QLabel(self.gridLayoutWidget)
        self.arduinoLabel.setObjectName(u"arduinoLabel")

        self.configureSlotsGrid.addWidget(self.arduinoLabel, 0, 3, 1, 1)

        self.hvPowerSupplyLabel = QLabel(self.gridLayoutWidget)
        self.hvPowerSupplyLabel.setObjectName(u"hvPowerSupplyLabel")

        self.configureSlotsGrid.addWidget(self.hvPowerSupplyLabel, 0, 2, 1, 1)

        self.slotNumberLabel = QLabel(self.gridLayoutWidget)
        self.slotNumberLabel.setObjectName(u"slotNumberLabel")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.slotNumberLabel.sizePolicy().hasHeightForWidth())
        self.slotNumberLabel.setSizePolicy(sizePolicy)
        self.slotNumberLabel.setMaximumSize(QSize(70, 35))

        self.configureSlotsGrid.addWidget(self.slotNumberLabel, 0, 0, 1, 1)

        self.arduinoLabel_2 = QLabel(self.gridLayoutWidget)
        self.arduinoLabel_2.setObjectName(u"arduinoLabel_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.arduinoLabel_2.sizePolicy().hasHeightForWidth())
        self.arduinoLabel_2.setSizePolicy(sizePolicy1)
        self.arduinoLabel_2.setMinimumSize(QSize(10, 0))
        self.arduinoLabel_2.setMaximumSize(QSize(70, 16777215))

        self.configureSlotsGrid.addWidget(self.arduinoLabel_2, 0, 4, 1, 1)

        self.slotLayout = QVBoxLayout()
        self.slotLayout.setObjectName(u"slotLayout")

        self.configureSlotsGrid.addLayout(self.slotLayout, 2, 0, 1, 5)

        self.cancelButton = QPushButton(self.gridLayoutWidget)
        self.cancelButton.setObjectName(u"cancelButton")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.cancelButton.sizePolicy().hasHeightForWidth())
        self.cancelButton.setSizePolicy(sizePolicy2)

        self.configureSlotsGrid.addWidget(self.cancelButton, 3, 3, 1, 2)

        self.saveConfigurationButton = QPushButton(self.gridLayoutWidget)
        self.saveConfigurationButton.setObjectName(u"saveConfigurationButton")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.saveConfigurationButton.sizePolicy().hasHeightForWidth())
        self.saveConfigurationButton.setSizePolicy(sizePolicy3)

        self.configureSlotsGrid.addWidget(self.saveConfigurationButton, 3, 0, 1, 3)


        self.retranslateUi(ConfigureSlotsDialog)

        QMetaObject.connectSlotsByName(ConfigureSlotsDialog)
    # setupUi

    def retranslateUi(self, ConfigureSlotsDialog):
        ConfigureSlotsDialog.setWindowTitle(QCoreApplication.translate("ConfigureSlotsDialog", u"Dialog", None))
        self.lvPowerSupplyLabel.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"LV Power supply", None))
        self.arduinoLabel.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"RH / T Arduino", None))
        self.hvPowerSupplyLabel.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"HV Power supply", None))
        self.slotNumberLabel.setText("")
        self.arduinoLabel_2.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"OG", None))
        self.cancelButton.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"Cancel", None))
        self.saveConfigurationButton.setText(QCoreApplication.translate("ConfigureSlotsDialog", u"Save", None))
    # retranslateUi

