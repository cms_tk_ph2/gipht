from Dialog.Ui_Monitor import Ui_Monitor
from PySide2.QtWidgets import QWidget, QPushButton
from PySide2.QtCore import QThread, Qt

from typing import *

from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.lines import Line2D
import matplotlib as mpl

styleList = ['-','--','-.',':']

class Monitor(QWidget):
    def __init__(self):
        super(Monitor, self).__init__()
        self.reinit()

    def reinit( self ):
        self.ui = Ui_Monitor()
        self.ui.setupUi(self)

        self.lvCanvas = MonitorCanvas(100,"Voltage (V)", "Current (A)",1000)
        self.hvCanvas = MonitorCanvas(100,"Voltage (V)", "Current (A)",1000)
        self.tRhCanvas = MonitorCanvas(100, "Temperature / Dewpoint (°C)", "Humidity (%)", 1000)

        self.ui.lowVoltageGraphLayout.addWidget(self.lvCanvas)
        self.ui.highVoltageGraphLayout.addWidget(self.hvCanvas)
        self.ui.tRhGraphLayout.addWidget(self.tRhCanvas)

        self.lvThread = MonitorThread(self.lvCanvas)
        self.hvThread = MonitorThread(self.hvCanvas)
        self.tRhThread = MonitorThread(self.tRhCanvas)

    def start( self ):
        self.lvThread.running = True
        self.hvThread.running = True
        self.tRhThread.running = True

        self.lvThread.start()
        self.hvThread.start()
        self.tRhThread.start()

    def stop( self ):
        self.lvThread.running = False
        self.hvThread.running = False
        self.tRhThread.running = False
        while self.lvThread.isRunning() or self.hvThread.isRunning() or self.tRhThread.isRunning():
            QThread.msleep(10)

    def linkDevices ( self, pDeviceList):
        for device in pDeviceList:
            self.addDeviceToMonitor(device)


    def addDeviceToMonitor( self, pDevice):
        if pDevice.config["Type"] == "PowerSupply":
            for channel in pDevice.channels:
                if channel.config['HVLV'] == "LV":
                    self.lvCanvas.x.append      ( channel.monitorData["Timestamp"]  )
                    self.lvCanvas.yL.append     ( channel.monitorData["Voltage"]    )
                    self.lvCanvas.yR.append     ( channel.monitorData["Current"]    )
                    self.lvCanvas.isOn.append   ( channel.monitorData["IsOn"]       )
                    self.lvCanvas.labels.append ( pDevice.config.get("ID","-") + " - " + channel.config.get("ID","-") )
                else:
                    self.hvCanvas.x.append      ( channel.monitorData["Timestamp"]  )
                    self.hvCanvas.yL.append     ( channel.monitorData["Voltage"]    )
                    self.hvCanvas.yR.append     ( channel.monitorData["Current"]    )   
                    self.hvCanvas.isOn.append   ( channel.monitorData["IsOn"]       )
                    self.hvCanvas.labels.append ( pDevice.config.get("ID","-") + " - " + channel.config.get("ID","-") )

        if pDevice.config["Type"] == "Arduino":
            self.tRhCanvas.x.append     ( pDevice.monitorData["Timestamp"]  )
            self.tRhCanvas.yL.append    ( pDevice.monitorData["temperature"])
            self.tRhCanvas.yL2.append   ( pDevice.monitorData["dewpoint"]   )
            self.tRhCanvas.yR.append    ( pDevice.monitorData["humidity"]   )
            self.tRhCanvas.labels.append( pDevice.config.get("ID","-")      )


class MonitorThread(QThread):
    def __init__(self, pCanvas):
        super(MonitorThread, self ).__init__(  )
        self.running = True
        self.canvas = pCanvas

    def run ( self ):
        while self.running:
            for i in range(50):            
                if self.running:
                    QThread.msleep(20)
                else:
                    return
            self.canvas.updateCanvas()

class MonitorCanvas(FigureCanvas):
    def __init__(self, pXMax, pYLLabel, pYRLabel , interval:int):
        super().__init__(mpl.figure.Figure())

        self.yLLabel = pYLLabel
        self.yRLabel = pYRLabel

        self.x      = []
        self.isOn   = []
        self.yL     = []
        self.yL2    = []
        self.yR     = []

        self.labels = []

        # Store a figure ax
        self.axL = self.figure.subplots()
        self.axL.set_xlabel("Time")
        self.axL.set_ylabel(self.yLLabel)

        self.axR = self.axL.twinx()
        self.axR.set_ylabel(self.yRLabel)

    def updateCanvas(self):
        for index, values in enumerate( self.yL ):
            if values and self.x[index] is not None:
                if len(values) == len ( self.x[index] ):
                    try:
                        self.axL.plot(self.x[index], values, color = 'b', linewidth = 0.8, linestyle = styleList[index % len(styleList)], marker = '.', markersize = 3)
                        self.axL.yaxis.label.set_color('b')
                        self.axL.spines['left'].set_color('b')
                        self.axL.tick_params(axis='y', colors = 'b')
                    except RuntimeError:
                        pass
                    except ValueError:
                        pass

        for index, values in enumerate( self.yL2 ):
            if values and self.x[index] is not None:
                if len(values) == len ( self.x[index] ):
                    try:
                        self.axL.plot(self.x[index], values, color = 'g', linewidth = 0.8, linestyle = styleList[index % len(styleList)], marker = '.', markersize = 3)
                        self.axL.yaxis.label.set_color('g')
                        self.axL.spines['left'].set_color('g')
                        self.axL.tick_params(axis='y', colors = 'g')
                    except RuntimeError:
                        pass
                    except ValueError:
                        pass

        for index, values in enumerate( self.yR ):
            if values and self.x[index] is not None:
                if len(values) == len ( self.x[index] ):
                    try:
                        self.axR.plot(self.x[index], values, color = 'r' , linewidth = 0.8, linestyle = styleList[index % len(styleList)], marker = '.', markersize = 3)
                        self.axR.yaxis.label.set_color('r')
                        self.axR.spines['right'].set_color('r')
                        self.axR.tick_params(axis='y', colors = 'r')
                    except RuntimeError:
                        pass
                    except ValueError:
                        pass

        customLegend = []
        for index in range ( len ( self.yL ) ):
            customLegend.append ( Line2D([0],[0], color = 'black', linestyle = styleList[index % len(styleList)] ) )

        self.axL.legend(customLegend, self.labels, loc="upper left")

        self.draw()   
