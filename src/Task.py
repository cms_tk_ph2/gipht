#------------------------------------------------
#+++++++++++++Measurement.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      26.11.2020
#------------------------------------------------
import os, yaml
from datetime import datetime
from PySide2.QtCore import Signal, QThread
from src.XMLManipulator import XMLManipulator
import time, subprocess


#Top class of all topLevel task items. Each measurement and each task is an own object with a set of signals.
class Task(QThread):
    taskDone        = Signal( object )  #Current task finished
    statusUpdate    = Signal( object )  #The status of the device changed
    progressUpdate  = Signal( object )  #There is progress on the current task
    logUpdate       = Signal( object )  #New logs are available

    askPermission   = Signal( object, object ) #Using GUI related thing in QThreads leads to segementation faults

    CGREEN = '\033[32m'     #For green prints
    CRED   = '\033[91m'     #For red prints
    CEND   = '\033[0m'      #End color prints

    def __init__( self, pIndex, pType, pWait = False ):
        super(Task, self).__init__(  )
        self.index = pIndex
        self.type = pType
        self.status         = "Not running"
        self.task           = pType
        self.progress       = 0

        #Is set by the GUIController to keep track which tasks are already done
        self.aborted        = False
        self.running        = False
        self.permission     = False
        self.done           = False
        self.waitForTask    = pWait

    def taskFinished( self ):
        self.done = True
        self.running = False
        self.setProgress( 100 )
        if self.aborted:
            self.setStatus("Done: Stopped")
        else:
            self.setStatus("Done")
        self.taskDone.emit( self )

    def setStatus( self, pStatus ):
        self.status = pStatus
        self.statusUpdate.emit ( self )

        if not self.type == "Check" and not "Done" in pStatus and not "New device" in pStatus:
            self.saveTimestamp(pStatus.replace("Running: ",""))

    def setProgress( self, pProgress ):
        self.progress = pProgress
        self.progressUpdate.emit ( self )

    def stopTask ( self ):
        self.running = False
        self.aborted = True
        self.setStatus("Done: Stopped")

    def saveTimestamp( self, pInformation ): #Stores the timestamp in a dedicated file
        file = os.getcwd() + "/logs/timestamps.yml"

        entries = []
        if os.path.isfile(file):
            with open(file, 'r') as f:
                entries = yaml.safe_load(f)
        if entries is None:
            entries = []
        entry = {"Index": len(entries), "Timestamp" : str(datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f") )[:-3], "Task": self.type, "Information": pInformation}
        entries.append(entry)

        with open(file, "w+") as f:
            yaml.dump(entries,f)


#-----------------------------------------------------------------------
#------------------------ TASK CLASSES --------------------------
#-----------------------------------------------------------------------
#Each task has its own class, to execute the corresponding commands

#Enables or disables one channel of a single power supply. Called by the power supply setting page
class Check(Task):
    def __init__( self, pIndex, pDevice, pWait = False ):
        super(Check, self).__init__(pIndex, "Check", pWait)
        print("GIPHT:\tNew Check task object for " + pDevice.type + " " + pDevice.config["ID"])
        self.device    = pDevice
        self.device.taskDone.connect( self.taskFinished )

    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Check device " + self.device.config["ID"])
        self.device.check()
        #Wait until the check task is done
        while self.running and not self.device.isIdle():
            QThread.msleep(100)
        
        self.taskFinished()

class SwitchFirmware(Task):

    def __init__( self, pIndex, pFc7, pFirmware ):
        super(SwitchFirmware, self).__init__(pIndex, "SwitchFirmware", True)
        print("GIPHT:\tNew SwitchFirmware task object for " + pFc7.type + " " + pFc7.config["ID"])
        self.fc7 = pFc7
        self.firmware = pFirmware
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Load new firmware on " + self.fc7.config["ID"])
        ip = self.fc7.config["IPAddress"]
        self.askPermission.emit(self, [ "Firmware", "Switch Firmware on\nFC7 " + ip + "\nto " + self.firmware+ " ?"])
            
        while not self.permission and self.running:
            QThread.msleep(100)

        if self.running:
            self.fc7.switchFirmware(self.firmware)
            #Wait until the check task is done
            while self.running and not self.fc7.isIdle():
                QThread.msleep(100)
        self.taskFinished()

class UploadFirmware(Task):

    def __init__( self, pIndex, pFc7, pFirmware ):
        super(UploadFirmware, self).__init__(pIndex, "UploadFirmware", True)
        print("GIPHT:\tNew UploadFirmware task object for " + pFc7.type + " " + pFc7.config["ID"])
        self.fc7 = pFc7
        self.firmware = pFirmware

    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Load new firmware on " + self.fc7.config["ID"])
        ip = self.fc7.config["IPAddress"]
        self.askPermission.emit(self, [ "Firmware", "Upload Firmware\n"+ self.firmware + "\nto FC7 " + ip +" ?"])
            
        while not self.permission and self.running:
            QThread.msleep(100)

        if self.running:
            self.fc7.uploadFirmware(self.firmware)
            #Wait until the check task is done
            while self.running and not self.fc7.isIdle():
                QThread.msleep(100)
        self.taskFinished()

class VTRXLightOff(Task):
    def __init__( self, pIndex, pSlot, pFC7, pConfigList):
        super(VTRXLightOff, self).__init__(pIndex, "VTRXLightOff", True)
        print("GIPHT:\tNew VTRXLightOff task object for " + pSlot.module.id)
        self.slot = pSlot
        self.configList = pConfigList
        self.fc7 = pFC7
        #GENERATE XML FILE for one module HERE
        self.xmlFile = os.getcwd() + "/settings/Tasks/LightOff_" + str(self.index) + ".xml"

        xmlEditor = XMLManipulator()
        #Create file for measurement
        #HERE IT would be necessar to differ between PS and 2S

        fileValid = ""
        if self.slot.getModuleType() == '2S':
            fileValid = xmlEditor.copyXmlFile   (self.xmlFile,  self.configList["Default2SHwFile"] )
        if 'PS' in self.slot.getModuleType():
            fileValid = xmlEditor.copyXmlFile   (self.xmlFile,  self.configList["DefaultPSHwFile"] )

        if fileValid:
            xmlEditor.exchangeIpAddress (self.xmlFile,  self.fc7.config["IPAddress"])
            xmlEditor.editModuleBlock(self.xmlFile, self.slot.og) #Last cahracter of OG entry
            #if self.slot.getModuleType() == '2S':
            #    xmlEditor.editLpGBTVersion(self.xmlFile, self.configList["lpGBT_Version"])

    def run( self ):

        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Switch off light of VTRX+ on module " + self.slot.module.id)

        if self.running:
            self.fc7.turnOffVTRXLight(self.xmlFile)

        self.taskFinished()



class EnableDisableChannel(Task):
    def __init__( self, pIndex, pPowerSupply, pChannelIndex ):
        super(EnableDisableChannel, self).__init__(pIndex, "EnableDisableChannel")
        print("GIPHT:\tNew EnableDisableChannel task object")
        self.powerSupply    = pPowerSupply
        self.channelIndex   = pChannelIndex

    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Enable/Disable channel " + self.powerSupply.channels[self.channelIndex].config["ID"] + " on " +self.powerSupply.config["ID"])


        self.powerSupply.enableDisableChannel(self.channelIndex)
        while self.running and not self.powerSupply.isIdle():
            QThread.msleep(100)
        self.taskFinished()

class EnableChannels(Task):
    def __init__( self, pIndex, pPowerSupplyAndChannels, pAskForPermission = True, pWaitForTask = False ):
        super(EnableChannels, self).__init__(pIndex, "EnableChannels", pWaitForTask)
        print("GIPHT:\tNew EnableChannels task object")
        self.powerSupplyAndChannels = pPowerSupplyAndChannels
        self.display = ""
        self.askForPermission = pAskForPermission
        for powerSupplyAndChannel in self.powerSupplyAndChannels:
            self.display += "\n" + powerSupplyAndChannel[0].config["ID"] + "(" + powerSupplyAndChannel[1].config["ID"] +")"
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Enable channels " +self.display.replace("\n",""))

        if self.askForPermission:
            self.askPermission.emit(self, [ "Voltage", "Enable channels" + self.display + "?"])
                
            while not self.permission and self.running:
                QThread.msleep(100)

        if self.running:
            for powerSupplyAndChannel in self.powerSupplyAndChannels:
                powerSupplyAndChannel[0].enableChannel(powerSupplyAndChannel[1].index)

            #Only continue of all power supplies are back on idle
            while self.running and not all( [ powerSupplyAndChannel[0].isIdle() for powerSupplyAndChannel in self.powerSupplyAndChannels ] ):
                QThread.msleep(100)
        self.taskFinished()

class DisableChannels(Task):
    def __init__( self, pIndex, pPowerSupplyAndChannels, pWaitForTask = False ):
        super(DisableChannels, self).__init__(pIndex, "DisableChannels", pWaitForTask)
        print("GIPHT:\tNew DisableChannels task object")
        self.powerSupplyAndChannels = pPowerSupplyAndChannels
        self.display = ""
        for powerSupplyAndChannel in self.powerSupplyAndChannels:
            self.display += "\n" + powerSupplyAndChannel[0].config["ID"] + "(" + powerSupplyAndChannel[1].config["ID"]+")"
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Disable channels " +self.display.replace("\n",""))

        #Permission not necessary for disabling
        #self.askPermission.emit(self, [ "Voltage", "Enable channels" + self.display + "?"])            
        #while not self.permission and self.running:
        #    QThread.msleep(100)

        if self.running:
            for powerSupplyAndChannel in self.powerSupplyAndChannels:
                powerSupplyAndChannel[0].disableChannel(powerSupplyAndChannel[1].index)

            #Only continue of all power supplies are back on idle
            while self.running and not all( [ powerSupplyAndChannel[0].isIdle() for powerSupplyAndChannel in self.powerSupplyAndChannels ] ):
                QThread.msleep(100)
        self.taskFinished()

class SetVoltages(Task):
    def __init__( self, pIndex, pPowerSuppliesChannelsVoltages, pAskForPermission = True, pWaitForTask = False ):
        super(SetVoltages, self).__init__(pIndex, "SetVoltages", pWaitForTask)
        print("GIPHT:\tNew SetVoltages task object")
        self.powerSuppliesChannelsVoltages    = pPowerSuppliesChannelsVoltages         #is a list of a list containing device, channel, voltage
        self.askForPermission = pAskForPermission
        self.display = ""
        for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
            self.display += "\n" + powerSupplyChannelVoltage[0].config["ID"] + "(" + powerSupplyChannelVoltage[1].config["ID"]+") : {:.2f}".format(powerSupplyChannelVoltage[2])


    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Set voltages to channels " + self.display.replace("\n",""))
        #print([ powerSupplyChannelVoltage[1].voltageInRange( powerSupplyChannelVoltage[2]) for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ])
        #Check for all channels whether the voltage is in the allowed range
        if all ( [ powerSupplyChannelVoltage[1].voltageInRange( powerSupplyChannelVoltage[2]) for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ] ):
            #Dont ask for permission if the set voltage is 0
            if not all( [powerSupplyChannelVoltage[2] == 0 for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                if self.askForPermission:
                    self.askPermission.emit(self, [ "Voltage", "Apply " + self.display + "?"])            

                    while not self.permission and self.running:
                        QThread.msleep(100)

            if self.running:
                while self.running and not all( [powerSupplyChannelVoltage[0].isIdle() for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                    QThread.msleep(100)
                for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
                    powerSupplyChannelVoltage[0].setVoltage(powerSupplyChannelVoltage[1].index, powerSupplyChannelVoltage[2])
                while self.running and not all( [powerSupplyChannelVoltage[0].isIdle() for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                    QThread.msleep(100)
                allSet = [False] #Just to enter the loop
                while not all(allSet) and self.running:
                    allSet = []
                    while self.running and not all( [powerSupplyChannelVoltage[0].isIdle() for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                        QThread.msleep(500)
                    for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
                        powerSupplyChannelVoltage[0].readoutPowerSupply()
                    for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
                        voltage = powerSupplyChannelVoltage[0].channels[powerSupplyChannelVoltage[1].index].status["Voltage"]
                        if powerSupplyChannelVoltage[0].config["Model"] == "CAEN":
                            allSet.append(True)
                        else:
                            allSet.append(powerSupplyChannelVoltage[2] - 1 < float(voltage) < powerSupplyChannelVoltage[2] + 1)
        else:
            print("GIPHT:\t" + self.CRED + "Voltage is not in allowed range for power supply!" + self.CEND)
        self.taskFinished()

class KIRASettings(Task):
    def __init__( self, pIndex, pDevice, pSettingsDict ):
        super(KIRASettings, self).__init__(pIndex, "KIRASettings")

        print("GIPHT:\tNew KIRASettings task object")
        self.device    = pDevice
        self.settings  = pSettingsDict
        self.keepRunning = False

    def run( self ):
        self.running = True
        self.setStatus("Running: KIRASettings " + self.device.config["ID"])
        self.setProgress ( 0 )
        if self.running:
            self.device.updateAndWriteKIRASettings(self.settings)
        self.taskFinished()

class KIRAOff(Task):
    def __init__( self, pIndex, pDevice, pWait ):
        super(KIRAOff, self).__init__(pIndex, "KIRAOff", pWait)
        print("GIPHT:\tNew KIRAOff task object")
        self.device    = pDevice
        self.keepRunning = False

        self.settings  = {}
        self.settings["Trigger"] =   "off"
        self.settings["TriggerFrequency"] = "0"
        self.settings["PulseLength"] = "100"
        self.settings["LEDs"] = []
        for i in range (16):
            led = {}
            led["Channel"]      = str(i)
            led["Intensity"]    = "0"
            led["Light"]        = "off"
            self.settings["LEDs"].append(led)

    def run( self ):
        self.running = True
        self.setStatus("Running: KIRAOff " + self.device.config["ID"])
        self.setProgress ( 0 )
        if self.running:
            self.device.updateAndWriteKIRASettings(self.settings)
            while self.running and not self.device.isIdle():
                QThread.msleep(100)
        self.taskFinished()

class CheckDB(Task):

    dbStatus = Signal ( object )
    def __init__( self, pIndex, pDB ):
        super(CheckDB, self).__init__(pIndex, "CheckDB", pWait = True)

        print("GIPHT:\tNew CheckDB task object")
        self.db = pDB

    def check(self):
        self.setStatus("Running: CheckDB" )
        status = False
        if not os.path.exists('.session.cache'):
            print("GIPHT:\tAsk for DB user credentials. Lets wait a few seconds...")
            time.sleep(3)
            print("GIPHT:\tPlease enter DB user credentials")
            time.sleep(0.1)
        try:
            if self.db.locations == []:
                self.dbStatus.emit("Offline")
            status = self.db.getLocations()
            self.db.locations = status
        except:
            status = False
        if status and status is not None:
            self.dbStatus.emit("Online")
        else:
            self.dbStatus.emit("Offline")

    def run( self ):
        self.running = True
        self.check()
        self.taskFinished()

class GetTestConditionsFromDB(Task):

    dbStatus = Signal ( object )
    def __init__( self, pIndex, pDB ):
        super(GetTestConditionsFromDB, self).__init__(pIndex, "GetTestConditionsFromDB")

        print("GIPHT:\tNew GetTestConditionsFromDB task object")
        self.db = pDB

    def query(self):
        self.setStatus("Running: GetTestConditionsFromDB" )
        status = False
        if not os.path.exists('.session.cache'):
            print("GIPHT:\tAsk for DB user credentials. Lets wait a few seconds...")
            time.sleep(3)
            print("GIPHT:\tPlease enter DB user credentials")
            time.sleep(0.1)
        try:
            if self.db.locations == []:
                self.dbStatus.emit("Offline")
            status = self.db.getTestConditions()
        #In case CB is not accessible
        except:
            status = False
        if status and status != {"PS":[], "2S": [], "Skeleton": []}:
            self.dbStatus.emit("Online")
        else:
            self.dbStatus.emit("Offline")

    def run( self ):
        self.running = True
        self.query()

        self.taskFinished()

class SEHROHlpGBTUpdate( Task ):
    def __init__( self, pIndex, pDB, pHybridId ):
        super(SEHROHlpGBTUpdate, self).__init__(pIndex, "SEHROHlpGBTUpdate")
        self.db = pDB
        self.hybrid_id = pHybridId
        self.lpgbt_id = "abc"
    def update(self):
        self.setStatus("Running: Update ROH / SEH of Module" )
        self.db.updateSEHROH(self.hybrid_id,self.lpgbt_id)

    def run( self ):
        self.running = True
        self.update()

        self.taskFinished()




class MonitorPoll(Task):
    def __init__( self, pIndex, pDevice, pDumpToFile = None ):
        super(MonitorPoll, self).__init__(pIndex, "MonitorPoll")

        print("GIPHT:\tNew MonitorPoll task object")
        self.device    = pDevice
        self.keepRunning = False
        self.dumpToFile = pDumpToFile

    def run( self ):
        self.running = True
        self.setStatus("Running: MonitorPoll " + self.device.config["ID"])
        
        while self.running:
            QThread.msleep(2000)
            diff = ( datetime.now() - self.device.lastReadout ).total_seconds() *1000
            #print(diff)
            if diff > 1000:
                if self.device.isIdle():
                    self.device.monitor(self.dumpToFile )
                while self.running and not self.device.isIdle():
                    QThread.msleep(100)

        self.taskFinished()

class WriteLogFile ( Task):
    def __init__( self, pIndex, pSlot, pRunInfo, pLogFilePath, pWaitForTask = False  ):
        super(WriteLogFile, self).__init__(pIndex, "WriteLogFile", pWaitForTask )    
        print("GIPHT:\tNew WriteLogFile task object")
        self.slot = pSlot
        self.runInfo = pRunInfo
        self.path = pLogFilePath

    def run( self ):
        self.running = True
        self.setStatus("Running: WriteLogFile")

        #Build string to append csv file: Run,Operator,Module,SEH/ROH,Type,Path,Treated

        sehroh = self.runInfo["Module_Parts"].get("rohSerial", "")
        if sehroh == "": sehroh = self.runInfo["Module_Parts"].get("sehSerial", "")

        test_type = ""
        if self.slot.module.skeleton:
            test_type = "skeleton"
        elif self.slot.module.encapsulated:
            test_type = "encapsulated"
        else:
            test_type = "unencapsulated"

        entryString = str(self.runInfo["LocalRunNumber"]) + "," \
                    + str(self.runInfo["Date"]) + "," \
                    + str(self.runInfo["Operator"]) + "," \
                    + str(self.runInfo["Module_ID"]) + "," \
                    + sehroh + "," \
                    + test_type + "," \
                    + str(self.runInfo["Ph2_ACF_Test"]) +  "," \
                    + str(self.runInfo["Result_Folder"]) + "," \
                    + "False"

        with open(self.path, "a") as logFile:
            logFile.write("\n" + entryString)

        self.taskFinished()
   

class RunCustomScript ( Task):
    def __init__( self, pIndex, pCmd,pWaitForTask = False  ):
        super(RunCustomScript, self).__init__(pIndex, "RunCustomScript", pWaitForTask )    
        print("GIPHT:\tNew RunCustomScript task object")
        self.cmd = pCmd

    def run( self ):
        self.running = True
        self.setStatus("Running: RunCustomScript")

        #Capture return code from the buffer
        if self.cmd != "":
            process = subprocess.Popen(self.cmd, executable="/bin/bash", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, err = process.communicate()
            print("GIPHT:\t Run custom script with command " +  str(self.cmd))
            print(stdout.decode())

        self.taskFinished()   