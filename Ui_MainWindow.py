# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1599, 834)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralTabWidget = QTabWidget(self.centralwidget)
        self.centralTabWidget.setObjectName(u"centralTabWidget")
        self.centralTabWidget.setGeometry(QRect(10, 10, 1571, 791))
        self.MeasurementTab = QWidget()
        self.MeasurementTab.setObjectName(u"MeasurementTab")
        self.measurementWidget = QWidget(self.MeasurementTab)
        self.measurementWidget.setObjectName(u"measurementWidget")
        self.measurementWidget.setGeometry(QRect(470, 10, 1081, 691))
        self.gridLayoutWidget_2 = QWidget(self.measurementWidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(10, 10, 1061, 671))
        self.MeasurementConfiguration = QGridLayout(self.gridLayoutWidget_2)
        self.MeasurementConfiguration.setObjectName(u"MeasurementConfiguration")
        self.MeasurementConfiguration.setContentsMargins(0, 0, 0, 0)
        self.slotLayout = QVBoxLayout()
        self.slotLayout.setObjectName(u"slotLayout")

        self.MeasurementConfiguration.addLayout(self.slotLayout, 3, 0, 1, 5)

        self.startMeasurementButton = QPushButton(self.gridLayoutWidget_2)
        self.startMeasurementButton.setObjectName(u"startMeasurementButton")
        self.startMeasurementButton.setStyleSheet(u"color:green")

        self.MeasurementConfiguration.addWidget(self.startMeasurementButton, 7, 4, 1, 1)

        self.taskProgressBarLabel = QLabel(self.gridLayoutWidget_2)
        self.taskProgressBarLabel.setObjectName(u"taskProgressBarLabel")

        self.MeasurementConfiguration.addWidget(self.taskProgressBarLabel, 9, 0, 1, 1)

        self.quickTestCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.quickTestCheckBox.setObjectName(u"quickTestCheckBox")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.quickTestCheckBox.sizePolicy().hasHeightForWidth())
        self.quickTestCheckBox.setSizePolicy(sizePolicy)

        self.MeasurementConfiguration.addWidget(self.quickTestCheckBox, 0, 4, 1, 1)

        self.labelPS = QLabel(self.gridLayoutWidget_2)
        self.labelPS.setObjectName(u"labelPS")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.labelPS.sizePolicy().hasHeightForWidth())
        self.labelPS.setSizePolicy(sizePolicy1)
        self.labelPS.setMinimumSize(QSize(0, 35))
        self.labelPS.setLayoutDirection(Qt.LeftToRight)
        self.labelPS.setAlignment(Qt.AlignCenter)

        self.MeasurementConfiguration.addWidget(self.labelPS, 0, 3, 1, 1)

        self.stopRunButton = QPushButton(self.gridLayoutWidget_2)
        self.stopRunButton.setObjectName(u"stopRunButton")
        self.stopRunButton.setStyleSheet(u"color:red")

        self.MeasurementConfiguration.addWidget(self.stopRunButton, 11, 4, 1, 1)

        self.stopTaskButton = QPushButton(self.gridLayoutWidget_2)
        self.stopTaskButton.setObjectName(u"stopTaskButton")
        self.stopTaskButton.setStyleSheet(u"color:red")

        self.MeasurementConfiguration.addWidget(self.stopTaskButton, 9, 4, 1, 1)

        self.runProgressBarLabel = QLabel(self.gridLayoutWidget_2)
        self.runProgressBarLabel.setObjectName(u"runProgressBarLabel")

        self.MeasurementConfiguration.addWidget(self.runProgressBarLabel, 11, 0, 1, 1)

        self.runProgressBar = QProgressBar(self.gridLayoutWidget_2)
        self.runProgressBar.setObjectName(u"runProgressBar")
        self.runProgressBar.setValue(0)

        self.MeasurementConfiguration.addWidget(self.runProgressBar, 11, 1, 1, 3)

        self.taskProgressBar = QProgressBar(self.gridLayoutWidget_2)
        self.taskProgressBar.setObjectName(u"taskProgressBar")
        self.taskProgressBar.setValue(0)

        self.MeasurementConfiguration.addWidget(self.taskProgressBar, 9, 1, 1, 3)

        self.fullTestCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.fullTestCheckBox.setObjectName(u"fullTestCheckBox")
        sizePolicy.setHeightForWidth(self.fullTestCheckBox.sizePolicy().hasHeightForWidth())
        self.fullTestCheckBox.setSizePolicy(sizePolicy)

        self.MeasurementConfiguration.addWidget(self.fullTestCheckBox, 1, 4, 1, 1)

        self.moduleTestComboBoxPS = QComboBox(self.gridLayoutWidget_2)
        self.moduleTestComboBoxPS.setObjectName(u"moduleTestComboBoxPS")

        self.MeasurementConfiguration.addWidget(self.moduleTestComboBoxPS, 1, 3, 1, 1)

        self.moduleTestComboBox2S = QComboBox(self.gridLayoutWidget_2)
        self.moduleTestComboBox2S.setObjectName(u"moduleTestComboBox2S")

        self.MeasurementConfiguration.addWidget(self.moduleTestComboBox2S, 1, 2, 1, 1)

        self.taskListLayout = QVBoxLayout()
        self.taskListLayout.setObjectName(u"taskListLayout")
        self.taskTreeWidget = QTreeWidget(self.gridLayoutWidget_2)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.taskTreeWidget.setHeaderItem(__qtreewidgetitem)
        self.taskTreeWidget.setObjectName(u"taskTreeWidget")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.taskTreeWidget.sizePolicy().hasHeightForWidth())
        self.taskTreeWidget.setSizePolicy(sizePolicy2)

        self.taskListLayout.addWidget(self.taskTreeWidget)


        self.MeasurementConfiguration.addLayout(self.taskListLayout, 6, 1, 1, 4)

        self.IVCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.IVCheckBox.setObjectName(u"IVCheckBox")
        sizePolicy.setHeightForWidth(self.IVCheckBox.sizePolicy().hasHeightForWidth())
        self.IVCheckBox.setSizePolicy(sizePolicy)

        self.MeasurementConfiguration.addWidget(self.IVCheckBox, 1, 1, 1, 1)

        self.empty = QLabel(self.gridLayoutWidget_2)
        self.empty.setObjectName(u"empty")
        sizePolicy.setHeightForWidth(self.empty.sizePolicy().hasHeightForWidth())
        self.empty.setSizePolicy(sizePolicy)
        self.empty.setMinimumSize(QSize(0, 35))
        self.empty.setLayoutDirection(Qt.LeftToRight)
        self.empty.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.MeasurementConfiguration.addWidget(self.empty, 1, 0, 1, 1)

        self.label2S = QLabel(self.gridLayoutWidget_2)
        self.label2S.setObjectName(u"label2S")
        sizePolicy1.setHeightForWidth(self.label2S.sizePolicy().hasHeightForWidth())
        self.label2S.setSizePolicy(sizePolicy1)
        self.label2S.setMinimumSize(QSize(0, 35))
        self.label2S.setLayoutDirection(Qt.LeftToRight)
        self.label2S.setAlignment(Qt.AlignCenter)

        self.MeasurementConfiguration.addWidget(self.label2S, 0, 2, 1, 1)

        self.fc7LogSettingsWidget = QWidget(self.MeasurementTab)
        self.fc7LogSettingsWidget.setObjectName(u"fc7LogSettingsWidget")
        self.fc7LogSettingsWidget.setGeometry(QRect(10, 100, 451, 601))
        self.gridLayoutWidget_5 = QWidget(self.fc7LogSettingsWidget)
        self.gridLayoutWidget_5.setObjectName(u"gridLayoutWidget_5")
        self.gridLayoutWidget_5.setGeometry(QRect(10, 10, 431, 155))
        self.GeneralInformationGrid = QGridLayout(self.gridLayoutWidget_5)
        self.GeneralInformationGrid.setObjectName(u"GeneralInformationGrid")
        self.GeneralInformationGrid.setContentsMargins(0, 0, 0, 0)
        self.locationComboBox = QComboBox(self.gridLayoutWidget_5)
        self.locationComboBox.setObjectName(u"locationComboBox")

        self.GeneralInformationGrid.addWidget(self.locationComboBox, 1, 2, 1, 2)

        self.locationLabel = QLabel(self.gridLayoutWidget_5)
        self.locationLabel.setObjectName(u"locationLabel")
        font = QFont()
        font.setPointSize(15)
        self.locationLabel.setFont(font)

        self.GeneralInformationGrid.addWidget(self.locationLabel, 1, 0, 1, 1)

        self.operatorComboBox = QComboBox(self.gridLayoutWidget_5)
        self.operatorComboBox.setObjectName(u"operatorComboBox")

        self.GeneralInformationGrid.addWidget(self.operatorComboBox, 0, 2, 1, 2)

        self.dbStatusDisplayLabel = QLabel(self.gridLayoutWidget_5)
        self.dbStatusDisplayLabel.setObjectName(u"dbStatusDisplayLabel")
        self.dbStatusDisplayLabel.setFont(font)

        self.GeneralInformationGrid.addWidget(self.dbStatusDisplayLabel, 3, 1, 1, 1)

        self.operatorLabel = QLabel(self.gridLayoutWidget_5)
        self.operatorLabel.setObjectName(u"operatorLabel")
        self.operatorLabel.setFont(font)

        self.GeneralInformationGrid.addWidget(self.operatorLabel, 0, 0, 1, 1)

        self.addOperatorButton = QPushButton(self.gridLayoutWidget_5)
        self.addOperatorButton.setObjectName(u"addOperatorButton")

        self.GeneralInformationGrid.addWidget(self.addOperatorButton, 0, 1, 1, 1)

        self.dbStatusLabel_2 = QLabel(self.gridLayoutWidget_5)
        self.dbStatusLabel_2.setObjectName(u"dbStatusLabel_2")
        self.dbStatusLabel_2.setFont(font)

        self.GeneralInformationGrid.addWidget(self.dbStatusLabel_2, 3, 0, 1, 1)

        self.locationLabel_2 = QLabel(self.gridLayoutWidget_5)
        self.locationLabel_2.setObjectName(u"locationLabel_2")
        self.locationLabel_2.setFont(font)

        self.GeneralInformationGrid.addWidget(self.locationLabel_2, 2, 0, 1, 1)

        self.stationNameLineEdit = QLineEdit(self.gridLayoutWidget_5)
        self.stationNameLineEdit.setObjectName(u"stationNameLineEdit")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.stationNameLineEdit.sizePolicy().hasHeightForWidth())
        self.stationNameLineEdit.setSizePolicy(sizePolicy3)

        self.GeneralInformationGrid.addWidget(self.stationNameLineEdit, 2, 2, 1, 2)

        self.gridLayoutWidget_4 = QWidget(self.fc7LogSettingsWidget)
        self.gridLayoutWidget_4.setObjectName(u"gridLayoutWidget_4")
        self.gridLayoutWidget_4.setGeometry(QRect(10, 190, 431, 401))
        self.plotLayout = QGridLayout(self.gridLayoutWidget_4)
        self.plotLayout.setObjectName(u"plotLayout")
        self.plotLayout.setContentsMargins(0, 0, 0, 0)
        self.GiphtIcon = QLabel(self.MeasurementTab)
        self.GiphtIcon.setObjectName(u"GiphtIcon")
        self.GiphtIcon.setGeometry(QRect(10, 0, 100, 100))
        self.GiphtIcon.setPixmap(QPixmap(u"Utils/gift.png"))
        self.GiphtIcon.setScaledContents(True)
        self.GiphtIcon.setAlignment(Qt.AlignCenter)
        self.GiphtIcon.setMargin(5)
        self.gridLayoutWidget_6 = QWidget(self.MeasurementTab)
        self.gridLayoutWidget_6.setObjectName(u"gridLayoutWidget_6")
        self.gridLayoutWidget_6.setGeometry(QRect(140, 20, 311, 80))
        self.runSlotsGridLayout = QGridLayout(self.gridLayoutWidget_6)
        self.runSlotsGridLayout.setObjectName(u"runSlotsGridLayout")
        self.runSlotsGridLayout.setContentsMargins(0, 0, 0, 0)
        self.runNumberLabel = QLabel(self.gridLayoutWidget_6)
        self.runNumberLabel.setObjectName(u"runNumberLabel")

        self.runSlotsGridLayout.addWidget(self.runNumberLabel, 1, 0, 1, 1)

        self.slotsLabel = QLabel(self.gridLayoutWidget_6)
        self.slotsLabel.setObjectName(u"slotsLabel")

        self.runSlotsGridLayout.addWidget(self.slotsLabel, 3, 0, 1, 1)

        self.slotsSpinBox = QSpinBox(self.gridLayoutWidget_6)
        self.slotsSpinBox.setObjectName(u"slotsSpinBox")

        self.runSlotsGridLayout.addWidget(self.slotsSpinBox, 3, 1, 1, 1)

        self.runNumberLabelValue = QLabel(self.gridLayoutWidget_6)
        self.runNumberLabelValue.setObjectName(u"runNumberLabelValue")

        self.runSlotsGridLayout.addWidget(self.runNumberLabelValue, 1, 1, 1, 1)

        self.configureSlotsButton = QPushButton(self.gridLayoutWidget_6)
        self.configureSlotsButton.setObjectName(u"configureSlotsButton")

        self.runSlotsGridLayout.addWidget(self.configureSlotsButton, 3, 2, 1, 1)

        self.centralTabWidget.addTab(self.MeasurementTab, "")
        self.ResultsTab = QWidget()
        self.ResultsTab.setObjectName(u"ResultsTab")
        self.resultsTreeWidget = QTreeWidget(self.ResultsTab)
        __qtreewidgetitem1 = QTreeWidgetItem()
        __qtreewidgetitem1.setText(0, u"1");
        self.resultsTreeWidget.setHeaderItem(__qtreewidgetitem1)
        self.resultsTreeWidget.setObjectName(u"resultsTreeWidget")
        self.resultsTreeWidget.setGeometry(QRect(10, 50, 1461, 681))
        self.formatForPotatoButton = QPushButton(self.ResultsTab)
        self.formatForPotatoButton.setObjectName(u"formatForPotatoButton")
        self.formatForPotatoButton.setGeometry(QRect(220, 10, 151, 36))
        self.loadResultFolderButton = QPushButton(self.ResultsTab)
        self.loadResultFolderButton.setObjectName(u"loadResultFolderButton")
        self.loadResultFolderButton.setGeometry(QRect(1330, 10, 141, 36))
        self.uploadResultsButton = QPushButton(self.ResultsTab)
        self.uploadResultsButton.setObjectName(u"uploadResultsButton")
        self.uploadResultsButton.setGeometry(QRect(610, 10, 111, 36))
        self.convertResultsButton = QPushButton(self.ResultsTab)
        self.convertResultsButton.setObjectName(u"convertResultsButton")
        self.convertResultsButton.setGeometry(QRect(430, 10, 121, 36))
        self.arrow_1 = QLabel(self.ResultsTab)
        self.arrow_1.setObjectName(u"arrow_1")
        self.arrow_1.setGeometry(QRect(170, 10, 41, 31))
        self.arrow_1.setPixmap(QPixmap(u"Utils/arrow.png"))
        self.arrow_1.setScaledContents(True)
        self.updateSEHROHButton = QPushButton(self.ResultsTab)
        self.updateSEHROHButton.setObjectName(u"updateSEHROHButton")
        self.updateSEHROHButton.setGeometry(QRect(10, 10, 151, 36))
        self.arrow_2 = QLabel(self.ResultsTab)
        self.arrow_2.setObjectName(u"arrow_2")
        self.arrow_2.setGeometry(QRect(380, 10, 41, 31))
        self.arrow_2.setPixmap(QPixmap(u"Utils/arrow.png"))
        self.arrow_2.setScaledContents(True)
        self.arrow_3 = QLabel(self.ResultsTab)
        self.arrow_3.setObjectName(u"arrow_3")
        self.arrow_3.setGeometry(QRect(560, 10, 41, 31))
        self.arrow_3.setPixmap(QPixmap(u"Utils/arrow.png"))
        self.arrow_3.setScaledContents(True)
        self.fullSequenceButton = QPushButton(self.ResultsTab)
        self.fullSequenceButton.setObjectName(u"fullSequenceButton")
        self.fullSequenceButton.setGeometry(QRect(950, 10, 121, 36))
        font1 = QFont()
        font1.setBold(True)
        font1.setUnderline(True)
        font1.setWeight(75)
        self.fullSequenceButton.setFont(font1)
        self.centralTabWidget.addTab(self.ResultsTab, "")
        self.ConfigurationTab = QWidget()
        self.ConfigurationTab.setObjectName(u"ConfigurationTab")
        self.gridLayoutWidget = QWidget(self.ConfigurationTab)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 871, 383))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.openDialogResultsFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogResultsFolderButton.setObjectName(u"openDialogResultsFolderButton")
        icon = QIcon()
        icon.addFile(u"Utils/folder.jpg", QSize(), QIcon.Normal, QIcon.Off)
        self.openDialogResultsFolderButton.setIcon(icon)
        self.openDialogResultsFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogResultsFolderButton, 4, 2, 1, 1)

        self.devicePackageFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.devicePackageFolderLineEdit.setObjectName(u"devicePackageFolderLineEdit")

        self.gridLayout.addWidget(self.devicePackageFolderLineEdit, 1, 1, 1, 1)

        self.openDialogPh2ACFFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogPh2ACFFolderButton.setObjectName(u"openDialogPh2ACFFolderButton")
        self.openDialogPh2ACFFolderButton.setIcon(icon)
        self.openDialogPh2ACFFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogPh2ACFFolderButton, 0, 2, 1, 1)

        self.checkControlhub = QPushButton(self.gridLayoutWidget)
        self.checkControlhub.setObjectName(u"checkControlhub")
        sizePolicy1.setHeightForWidth(self.checkControlhub.sizePolicy().hasHeightForWidth())
        self.checkControlhub.setSizePolicy(sizePolicy1)
        self.checkControlhub.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.checkControlhub, 0, 4, 1, 1)

        self.resultFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.resultFolderLineEdit.setObjectName(u"resultFolderLineEdit")

        self.gridLayout.addWidget(self.resultFolderLineEdit, 4, 1, 1, 1)

        self.resultsFolderLabel = QLabel(self.gridLayoutWidget)
        self.resultsFolderLabel.setObjectName(u"resultsFolderLabel")

        self.gridLayout.addWidget(self.resultsFolderLabel, 4, 0, 1, 1)

        self.ph2AcfFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.ph2AcfFolderLineEdit.setObjectName(u"ph2AcfFolderLineEdit")

        self.gridLayout.addWidget(self.ph2AcfFolderLineEdit, 0, 1, 1, 1)

        self.ph2AcfLabel = QLabel(self.gridLayoutWidget)
        self.ph2AcfLabel.setObjectName(u"ph2AcfLabel")

        self.gridLayout.addWidget(self.ph2AcfLabel, 0, 0, 1, 1)

        self.default2SXmlLineEdit = QLineEdit(self.gridLayoutWidget)
        self.default2SXmlLineEdit.setObjectName(u"default2SXmlLineEdit")

        self.gridLayout.addWidget(self.default2SXmlLineEdit, 2, 1, 1, 1)

        self.openDialogDefault2SXmlFileButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDefault2SXmlFileButton.setObjectName(u"openDialogDefault2SXmlFileButton")
        self.openDialogDefault2SXmlFileButton.setIcon(icon)
        self.openDialogDefault2SXmlFileButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDefault2SXmlFileButton, 2, 2, 1, 1)

        self.defaultPSXmlLabel = QLabel(self.gridLayoutWidget)
        self.defaultPSXmlLabel.setObjectName(u"defaultPSXmlLabel")

        self.gridLayout.addWidget(self.defaultPSXmlLabel, 3, 0, 1, 1)

        self.defaultPSXmlLineEdit = QLineEdit(self.gridLayoutWidget)
        self.defaultPSXmlLineEdit.setObjectName(u"defaultPSXmlLineEdit")

        self.gridLayout.addWidget(self.defaultPSXmlLineEdit, 3, 1, 1, 1)

        self.openDialogDevicePackageFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDevicePackageFolderButton.setObjectName(u"openDialogDevicePackageFolderButton")
        self.openDialogDevicePackageFolderButton.setIcon(icon)
        self.openDialogDevicePackageFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDevicePackageFolderButton, 1, 2, 1, 1)

        self.ph2AcfLabel_2 = QLabel(self.gridLayoutWidget)
        self.ph2AcfLabel_2.setObjectName(u"ph2AcfLabel_2")

        self.gridLayout.addWidget(self.ph2AcfLabel_2, 1, 0, 1, 1)

        self.openDefaultPSXmlButton = QPushButton(self.gridLayoutWidget)
        self.openDefaultPSXmlButton.setObjectName(u"openDefaultPSXmlButton")
        self.openDefaultPSXmlButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.openDefaultPSXmlButton, 3, 3, 1, 1)

        self.startControlhubButton = QPushButton(self.gridLayoutWidget)
        self.startControlhubButton.setObjectName(u"startControlhubButton")
        sizePolicy1.setHeightForWidth(self.startControlhubButton.sizePolicy().hasHeightForWidth())
        self.startControlhubButton.setSizePolicy(sizePolicy1)
        self.startControlhubButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.startControlhubButton, 1, 4, 1, 1)

        self.default2SXmlLabel = QLabel(self.gridLayoutWidget)
        self.default2SXmlLabel.setObjectName(u"default2SXmlLabel")

        self.gridLayout.addWidget(self.default2SXmlLabel, 2, 0, 1, 1)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.invertPlotCheckBox = QCheckBox(self.gridLayoutWidget)
        self.invertPlotCheckBox.setObjectName(u"invertPlotCheckBox")
        sizePolicy.setHeightForWidth(self.invertPlotCheckBox.sizePolicy().hasHeightForWidth())
        self.invertPlotCheckBox.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.invertPlotCheckBox, 0, 1, 1, 1)

        self.powerSupplyDialogCheckBox = QCheckBox(self.gridLayoutWidget)
        self.powerSupplyDialogCheckBox.setObjectName(u"powerSupplyDialogCheckBox")
        sizePolicy.setHeightForWidth(self.powerSupplyDialogCheckBox.sizePolicy().hasHeightForWidth())
        self.powerSupplyDialogCheckBox.setSizePolicy(sizePolicy)
        self.powerSupplyDialogCheckBox.setMinimumSize(QSize(0, 0))

        self.gridLayout_2.addWidget(self.powerSupplyDialogCheckBox, 1, 1, 1, 1)

        self.autoCheckStartCheckBox = QCheckBox(self.gridLayoutWidget)
        self.autoCheckStartCheckBox.setObjectName(u"autoCheckStartCheckBox")
        sizePolicy.setHeightForWidth(self.autoCheckStartCheckBox.sizePolicy().hasHeightForWidth())
        self.autoCheckStartCheckBox.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.autoCheckStartCheckBox, 1, 0, 1, 1)

        self.showPlotCheckBox = QCheckBox(self.gridLayoutWidget)
        self.showPlotCheckBox.setObjectName(u"showPlotCheckBox")
        sizePolicy.setHeightForWidth(self.showPlotCheckBox.sizePolicy().hasHeightForWidth())
        self.showPlotCheckBox.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.showPlotCheckBox, 0, 0, 1, 1)

        self.vtrxLightOffCheckBox = QCheckBox(self.gridLayoutWidget)
        self.vtrxLightOffCheckBox.setObjectName(u"vtrxLightOffCheckBox")

        self.gridLayout_2.addWidget(self.vtrxLightOffCheckBox, 0, 2, 1, 1)

        self.checkFWCheckBox = QCheckBox(self.gridLayoutWidget)
        self.checkFWCheckBox.setObjectName(u"checkFWCheckBox")

        self.gridLayout_2.addWidget(self.checkFWCheckBox, 1, 2, 1, 1)

        self.startMonitorCheckBox = QCheckBox(self.gridLayoutWidget)
        self.startMonitorCheckBox.setObjectName(u"startMonitorCheckBox")

        self.gridLayout_2.addWidget(self.startMonitorCheckBox, 0, 3, 1, 1)


        self.gridLayout.addLayout(self.gridLayout_2, 6, 0, 1, 4)

        self.openDefault2SXmlButton = QPushButton(self.gridLayoutWidget)
        self.openDefault2SXmlButton.setObjectName(u"openDefault2SXmlButton")
        self.openDefault2SXmlButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.openDefault2SXmlButton, 2, 3, 1, 1)

        self.restartDeviceButton = QPushButton(self.gridLayoutWidget)
        self.restartDeviceButton.setObjectName(u"restartDeviceButton")

        self.gridLayout.addWidget(self.restartDeviceButton, 6, 4, 1, 1)

        self.checkPh2Acf = QPushButton(self.gridLayoutWidget)
        self.checkPh2Acf.setObjectName(u"checkPh2Acf")
        self.checkPh2Acf.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.checkPh2Acf, 0, 3, 1, 1)

        self.openDialogDefaultPSXmlFileButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDefaultPSXmlFileButton.setObjectName(u"openDialogDefaultPSXmlFileButton")
        self.openDialogDefaultPSXmlFileButton.setIcon(icon)
        self.openDialogDefaultPSXmlFileButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDefaultPSXmlFileButton, 3, 2, 1, 1)

        self.resultsFolderLabel_2 = QLabel(self.gridLayoutWidget)
        self.resultsFolderLabel_2.setObjectName(u"resultsFolderLabel_2")

        self.gridLayout.addWidget(self.resultsFolderLabel_2, 5, 0, 1, 1)

        self.potatoFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.potatoFolderLineEdit.setObjectName(u"potatoFolderLineEdit")

        self.gridLayout.addWidget(self.potatoFolderLineEdit, 5, 1, 1, 1)

        self.openDialogPotatoFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogPotatoFolderButton.setObjectName(u"openDialogPotatoFolderButton")
        icon1 = QIcon()
        icon1.addFile(u"Utils/potato.jpeg", QSize(), QIcon.Normal, QIcon.Off)
        self.openDialogPotatoFolderButton.setIcon(icon1)
        self.openDialogPotatoFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogPotatoFolderButton, 5, 2, 1, 1)

        self.gridLayoutWidget_7 = QWidget(self.ConfigurationTab)
        self.gridLayoutWidget_7.setObjectName(u"gridLayoutWidget_7")
        self.gridLayoutWidget_7.setGeometry(QRect(10, 400, 881, 341))
        self.devicesControlGrid = QGridLayout(self.gridLayoutWidget_7)
        self.devicesControlGrid.setObjectName(u"devicesControlGrid")
        self.devicesControlGrid.setContentsMargins(0, 0, 0, 0)
        self.addFc7Button = QPushButton(self.gridLayoutWidget_7)
        self.addFc7Button.setObjectName(u"addFc7Button")

        self.devicesControlGrid.addWidget(self.addFc7Button, 1, 0, 1, 1)

        self.removeDeviceButton = QPushButton(self.gridLayoutWidget_7)
        self.removeDeviceButton.setObjectName(u"removeDeviceButton")

        self.devicesControlGrid.addWidget(self.removeDeviceButton, 4, 0, 1, 1)

        self.addPowerSupplyButton = QPushButton(self.gridLayoutWidget_7)
        self.addPowerSupplyButton.setObjectName(u"addPowerSupplyButton")

        self.devicesControlGrid.addWidget(self.addPowerSupplyButton, 2, 0, 1, 1)

        self.addArduinoButton = QPushButton(self.gridLayoutWidget_7)
        self.addArduinoButton.setObjectName(u"addArduinoButton")

        self.devicesControlGrid.addWidget(self.addArduinoButton, 3, 0, 1, 1)

        self.GiphtIcon_2 = QLabel(self.gridLayoutWidget_7)
        self.GiphtIcon_2.setObjectName(u"GiphtIcon_2")
        self.GiphtIcon_2.setMaximumSize(QSize(100, 100))
        self.GiphtIcon_2.setPixmap(QPixmap(u"Utils/gift.png"))
        self.GiphtIcon_2.setScaledContents(True)
        self.GiphtIcon_2.setAlignment(Qt.AlignCenter)
        self.GiphtIcon_2.setMargin(5)

        self.devicesControlGrid.addWidget(self.GiphtIcon_2, 6, 0, 1, 1)

        self.deviceTreeWidget = QTreeWidget(self.gridLayoutWidget_7)
        __qtreewidgetitem2 = QTreeWidgetItem()
        __qtreewidgetitem2.setText(0, u"1");
        self.deviceTreeWidget.setHeaderItem(__qtreewidgetitem2)
        self.deviceTreeWidget.setObjectName(u"deviceTreeWidget")

        self.devicesControlGrid.addWidget(self.deviceTreeWidget, 1, 1, 6, 1)

        self.verticalLayoutWidget = QWidget(self.ConfigurationTab)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(900, 10, 571, 731))
        self.deviceConfigsLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.deviceConfigsLayout.setObjectName(u"deviceConfigsLayout")
        self.deviceConfigsLayout.setContentsMargins(0, 0, 0, 0)
        self.centralTabWidget.addTab(self.ConfigurationTab, "")
        self.MonitorTab = QWidget()
        self.MonitorTab.setObjectName(u"MonitorTab")
        self.gridLayoutWidget_8 = QWidget(self.MonitorTab)
        self.gridLayoutWidget_8.setObjectName(u"gridLayoutWidget_8")
        self.gridLayoutWidget_8.setGeometry(QRect(9, 9, 1451, 711))
        self.monitorLayout = QGridLayout(self.gridLayoutWidget_8)
        self.monitorLayout.setObjectName(u"monitorLayout")
        self.monitorLayout.setContentsMargins(0, 0, 0, 0)
        self.centralTabWidget.addTab(self.MonitorTab, "")
        self.ExpertTab = QWidget()
        self.ExpertTab.setObjectName(u"ExpertTab")
        self.gridLayoutWidget_3 = QWidget(self.ExpertTab)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(10, 10, 391, 261))
        self.dbSettingsGrid = QGridLayout(self.gridLayoutWidget_3)
        self.dbSettingsGrid.setObjectName(u"dbSettingsGrid")
        self.dbSettingsGrid.setContentsMargins(0, 0, 0, 0)
        self.initDbStartupLabel = QLabel(self.gridLayoutWidget_3)
        self.initDbStartupLabel.setObjectName(u"initDbStartupLabel")

        self.dbSettingsGrid.addWidget(self.initDbStartupLabel, 3, 0, 1, 1)

        self.dbStatusDisplayLabel_2 = QLabel(self.gridLayoutWidget_3)
        self.dbStatusDisplayLabel_2.setObjectName(u"dbStatusDisplayLabel_2")

        self.dbSettingsGrid.addWidget(self.dbStatusDisplayLabel_2, 0, 1, 1, 1)

        self.useDbToCheckModules = QLabel(self.gridLayoutWidget_3)
        self.useDbToCheckModules.setObjectName(u"useDbToCheckModules")

        self.dbSettingsGrid.addWidget(self.useDbToCheckModules, 2, 0, 1, 1)

        self.baserUrlLabel = QLabel(self.gridLayoutWidget_3)
        self.baserUrlLabel.setObjectName(u"baserUrlLabel")

        self.dbSettingsGrid.addWidget(self.baserUrlLabel, 5, 0, 1, 1)

        self.initDbHandlerOnStartupCheckBox = QCheckBox(self.gridLayoutWidget_3)
        self.initDbHandlerOnStartupCheckBox.setObjectName(u"initDbHandlerOnStartupCheckBox")

        self.dbSettingsGrid.addWidget(self.initDbHandlerOnStartupCheckBox, 3, 1, 1, 1)

        self.useDbToCheckModulesButton = QCheckBox(self.gridLayoutWidget_3)
        self.useDbToCheckModulesButton.setObjectName(u"useDbToCheckModulesButton")

        self.dbSettingsGrid.addWidget(self.useDbToCheckModulesButton, 2, 1, 1, 1)

        self.updateTestConditionsButton = QPushButton(self.gridLayoutWidget_3)
        self.updateTestConditionsButton.setObjectName(u"updateTestConditionsButton")

        self.dbSettingsGrid.addWidget(self.updateTestConditionsButton, 1, 1, 1, 1)

        self.dbStatusLabelName = QLabel(self.gridLayoutWidget_3)
        self.dbStatusLabelName.setObjectName(u"dbStatusLabelName")

        self.dbSettingsGrid.addWidget(self.dbStatusLabelName, 0, 0, 1, 1)

        self.checkDbButton = QPushButton(self.gridLayoutWidget_3)
        self.checkDbButton.setObjectName(u"checkDbButton")

        self.dbSettingsGrid.addWidget(self.checkDbButton, 1, 0, 1, 1)

        self.groupBoxDB = QGroupBox(self.gridLayoutWidget_3)
        self.groupBoxDB.setObjectName(u"groupBoxDB")
        sizePolicy4 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.groupBoxDB.sizePolicy().hasHeightForWidth())
        self.groupBoxDB.setSizePolicy(sizePolicy4)
        self.groupBoxDB.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.groupBoxDB.setFlat(False)
        self.groupBoxDB.setCheckable(False)
        self.testDBRadioButton = QRadioButton(self.groupBoxDB)
        self.testDBRadioButton.setObjectName(u"testDBRadioButton")
        self.testDBRadioButton.setGeometry(QRect(5, 35, 111, 26))
        self.testDBRadioButton.setAutoExclusive(True)
        self.productionDBRadioButton = QRadioButton(self.groupBoxDB)
        self.productionDBRadioButton.setObjectName(u"productionDBRadioButton")
        self.productionDBRadioButton.setGeometry(QRect(5, 5, 151, 26))
        self.productionDBRadioButton.setAutoExclusive(True)

        self.dbSettingsGrid.addWidget(self.groupBoxDB, 5, 1, 1, 1)

        self.TwoFACheckBoxLabel = QLabel(self.gridLayoutWidget_3)
        self.TwoFACheckBoxLabel.setObjectName(u"TwoFACheckBoxLabel")

        self.dbSettingsGrid.addWidget(self.TwoFACheckBoxLabel, 4, 0, 1, 1)

        self.TwoFACheckBox = QCheckBox(self.gridLayoutWidget_3)
        self.TwoFACheckBox.setObjectName(u"TwoFACheckBox")

        self.dbSettingsGrid.addWidget(self.TwoFACheckBox, 4, 1, 1, 1)

        self.testButton = QPushButton(self.ExpertTab)
        self.testButton.setObjectName(u"testButton")
        self.testButton.setGeometry(QRect(430, 60, 101, 41))
        self.expertModeCheckBox = QCheckBox(self.ExpertTab)
        self.expertModeCheckBox.setObjectName(u"expertModeCheckBox")
        self.expertModeCheckBox.setGeometry(QRect(430, 20, 151, 20))
        self.testConditionsTableWidget = QTableWidget(self.ExpertTab)
        self.testConditionsTableWidget.setObjectName(u"testConditionsTableWidget")
        self.testConditionsTableWidget.setGeometry(QRect(560, 10, 551, 511))
        self.customScriptCheckBox = QCheckBox(self.ExpertTab)
        self.customScriptCheckBox.setObjectName(u"customScriptCheckBox")
        self.customScriptCheckBox.setGeometry(QRect(30, 380, 241, 25))
        self.customScriptLineEdit = QLineEdit(self.ExpertTab)
        self.customScriptLineEdit.setObjectName(u"customScriptLineEdit")
        self.customScriptLineEdit.setGeometry(QRect(30, 410, 491, 36))
        self.centralTabWidget.addTab(self.ExpertTab, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.centralTabWidget.setCurrentIndex(4)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.startMeasurementButton.setText(QCoreApplication.translate("MainWindow", u"START", None))
        self.taskProgressBarLabel.setText(QCoreApplication.translate("MainWindow", u"Task", None))
        self.quickTestCheckBox.setText(QCoreApplication.translate("MainWindow", u"Quick Test", None))
        self.labelPS.setText(QCoreApplication.translate("MainWindow", u"PS", None))
        self.stopRunButton.setText(QCoreApplication.translate("MainWindow", u"STOP", None))
        self.stopTaskButton.setText(QCoreApplication.translate("MainWindow", u"STOP Task", None))
        self.runProgressBarLabel.setText(QCoreApplication.translate("MainWindow", u"Run", None))
        self.fullTestCheckBox.setText(QCoreApplication.translate("MainWindow", u"Full Test", None))
        self.IVCheckBox.setText(QCoreApplication.translate("MainWindow", u"IV", None))
        self.empty.setText("")
        self.label2S.setText(QCoreApplication.translate("MainWindow", u"2S", None))
        self.locationLabel.setText(QCoreApplication.translate("MainWindow", u"Location", None))
        self.dbStatusDisplayLabel.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.operatorLabel.setText(QCoreApplication.translate("MainWindow", u"Operator", None))
        self.addOperatorButton.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.dbStatusLabel_2.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.locationLabel_2.setText(QCoreApplication.translate("MainWindow", u"Station", None))
        self.stationNameLineEdit.setText("")
        self.GiphtIcon.setText("")
        self.runNumberLabel.setText(QCoreApplication.translate("MainWindow", u"Run:", None))
        self.slotsLabel.setText(QCoreApplication.translate("MainWindow", u"Slots", None))
        self.runNumberLabelValue.setText("")
        self.configureSlotsButton.setText(QCoreApplication.translate("MainWindow", u"Configure Slots", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.MeasurementTab), QCoreApplication.translate("MainWindow", u"Measurement", None))
        self.formatForPotatoButton.setText(QCoreApplication.translate("MainWindow", u"Format for POTATO", None))
        self.loadResultFolderButton.setText(QCoreApplication.translate("MainWindow", u"Load result folder", None))
        self.uploadResultsButton.setText(QCoreApplication.translate("MainWindow", u"Upload to DB", None))
        self.convertResultsButton.setText(QCoreApplication.translate("MainWindow", u"Convert for DB", None))
        self.arrow_1.setText("")
        self.updateSEHROHButton.setText(QCoreApplication.translate("MainWindow", u"Update SEH / ROH", None))
        self.arrow_2.setText("")
        self.arrow_3.setText("")
        self.fullSequenceButton.setText(QCoreApplication.translate("MainWindow", u"Full Sequence", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ResultsTab), QCoreApplication.translate("MainWindow", u"Results", None))
        self.openDialogResultsFolderButton.setText("")
        self.devicePackageFolderLineEdit.setText("")
        self.openDialogPh2ACFFolderButton.setText("")
        self.checkControlhub.setText(QCoreApplication.translate("MainWindow", u"Controlhub OFF \n"
" Check", None))
        self.resultFolderLineEdit.setText("")
        self.resultsFolderLabel.setText(QCoreApplication.translate("MainWindow", u"Results Folder", None))
        self.ph2AcfFolderLineEdit.setText("")
        self.ph2AcfLabel.setText(QCoreApplication.translate("MainWindow", u"Ph2_ACF Folder", None))
        self.default2SXmlLineEdit.setText("")
        self.openDialogDefault2SXmlFileButton.setText("")
        self.defaultPSXmlLabel.setText(QCoreApplication.translate("MainWindow", u"Default PS XML", None))
        self.defaultPSXmlLineEdit.setText("")
        self.openDialogDevicePackageFolderButton.setText("")
        self.ph2AcfLabel_2.setText(QCoreApplication.translate("MainWindow", u"Device Package", None))
        self.openDefaultPSXmlButton.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.startControlhubButton.setText(QCoreApplication.translate("MainWindow", u"Start Controlhub", None))
        self.default2SXmlLabel.setText(QCoreApplication.translate("MainWindow", u"Default 2S XML", None))
        self.invertPlotCheckBox.setText(QCoreApplication.translate("MainWindow", u"Invert IV plot", None))
        self.powerSupplyDialogCheckBox.setText(QCoreApplication.translate("MainWindow", u"Power supply dialogs", None))
        self.autoCheckStartCheckBox.setText(QCoreApplication.translate("MainWindow", u"Check devices on startup  ", None))
        self.showPlotCheckBox.setText(QCoreApplication.translate("MainWindow", u"Show IV plot", None))
        self.vtrxLightOffCheckBox.setText(QCoreApplication.translate("MainWindow", u"IV VTRX+ Light OFF", None))
        self.checkFWCheckBox.setText(QCoreApplication.translate("MainWindow", u"Check FW before Test", None))
        self.startMonitorCheckBox.setText(QCoreApplication.translate("MainWindow", u"Start Monitor", None))
        self.openDefault2SXmlButton.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.restartDeviceButton.setText(QCoreApplication.translate("MainWindow", u"Restart Devices", None))
        self.checkPh2Acf.setText(QCoreApplication.translate("MainWindow", u"Ph2_ACF not res-\n"
" ponsive Check", None))
        self.openDialogDefaultPSXmlFileButton.setText("")
        self.resultsFolderLabel_2.setText(QCoreApplication.translate("MainWindow", u"POTATO Folder", None))
        self.potatoFolderLineEdit.setText("")
        self.openDialogPotatoFolderButton.setText("")
        self.addFc7Button.setText(QCoreApplication.translate("MainWindow", u"Add FC7", None))
        self.removeDeviceButton.setText(QCoreApplication.translate("MainWindow", u"Remove Device", None))
        self.addPowerSupplyButton.setText(QCoreApplication.translate("MainWindow", u"Add PowerSupply", None))
        self.addArduinoButton.setText(QCoreApplication.translate("MainWindow", u"Add Arduino", None))
        self.GiphtIcon_2.setText("")
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ConfigurationTab), QCoreApplication.translate("MainWindow", u"Configuration", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.MonitorTab), QCoreApplication.translate("MainWindow", u"Monitor", None))
        self.initDbStartupLabel.setText(QCoreApplication.translate("MainWindow", u"Init DB  on Startup", None))
        self.dbStatusDisplayLabel_2.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.useDbToCheckModules.setText(QCoreApplication.translate("MainWindow", u"Use DB to check Modules", None))
        self.baserUrlLabel.setText(QCoreApplication.translate("MainWindow", u"DB", None))
        self.initDbHandlerOnStartupCheckBox.setText("")
        self.useDbToCheckModulesButton.setText("")
        self.updateTestConditionsButton.setText(QCoreApplication.translate("MainWindow", u"Update Test Conditions", None))
        self.dbStatusLabelName.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.checkDbButton.setText(QCoreApplication.translate("MainWindow", u"Init and Check DB", None))
        self.groupBoxDB.setTitle("")
        self.testDBRadioButton.setText(QCoreApplication.translate("MainWindow", u"Test DB", None))
        self.productionDBRadioButton.setText(QCoreApplication.translate("MainWindow", u"Production DB", None))
        self.TwoFACheckBoxLabel.setText(QCoreApplication.translate("MainWindow", u"2 Factor Authentification", None))
        self.TwoFACheckBox.setText("")
        self.testButton.setText(QCoreApplication.translate("MainWindow", u"Test", None))
        self.expertModeCheckBox.setText(QCoreApplication.translate("MainWindow", u"Expert Mode", None))
        self.customScriptCheckBox.setText(QCoreApplication.translate("MainWindow", u"Custom script at end of run", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ExpertTab), QCoreApplication.translate("MainWindow", u"Expert Tab", None))
    # retranslateUi

