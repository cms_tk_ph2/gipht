# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'LoginDB.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_LoginDialog(object):
    def setupUi(self, LoginDialog):
        if not LoginDialog.objectName():
            LoginDialog.setObjectName(u"LoginDialog")
        LoginDialog.resize(265, 156)
        LoginDialog.setSizeGripEnabled(True)
        self.gridLayoutWidget = QWidget(LoginDialog)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(15, 20, 231, 122))
        self.loginGrid = QGridLayout(self.gridLayoutWidget)
        self.loginGrid.setObjectName(u"loginGrid")
        self.loginGrid.setContentsMargins(0, 0, 0, 0)
        self.cancelButton = QPushButton(self.gridLayoutWidget)
        self.cancelButton.setObjectName(u"cancelButton")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cancelButton.sizePolicy().hasHeightForWidth())
        self.cancelButton.setSizePolicy(sizePolicy)
        self.cancelButton.setAutoDefault(False)

        self.loginGrid.addWidget(self.cancelButton, 2, 0, 1, 2)

        self.okButton = QPushButton(self.gridLayoutWidget)
        self.okButton.setObjectName(u"okButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.okButton.sizePolicy().hasHeightForWidth())
        self.okButton.setSizePolicy(sizePolicy1)
        self.okButton.setAutoDefault(False)

        self.loginGrid.addWidget(self.okButton, 2, 2, 1, 2)

        self.name = QLabel(self.gridLayoutWidget)
        self.name.setObjectName(u"name")

        self.loginGrid.addWidget(self.name, 0, 0, 1, 2)

        self.pw = QLabel(self.gridLayoutWidget)
        self.pw.setObjectName(u"pw")

        self.loginGrid.addWidget(self.pw, 1, 0, 1, 2)

        self.pwLineEdit = QLineEdit(self.gridLayoutWidget)
        self.pwLineEdit.setObjectName(u"pwLineEdit")

        self.loginGrid.addWidget(self.pwLineEdit, 1, 2, 1, 1)

        self.nameLineEdit = QLineEdit(self.gridLayoutWidget)
        self.nameLineEdit.setObjectName(u"nameLineEdit")

        self.loginGrid.addWidget(self.nameLineEdit, 0, 2, 1, 1)

        QWidget.setTabOrder(self.nameLineEdit, self.pwLineEdit)
        QWidget.setTabOrder(self.pwLineEdit, self.okButton)
        QWidget.setTabOrder(self.okButton, self.cancelButton)

        self.retranslateUi(LoginDialog)

        self.okButton.setDefault(True)


        QMetaObject.connectSlotsByName(LoginDialog)
    # setupUi

    def retranslateUi(self, LoginDialog):
        LoginDialog.setWindowTitle(QCoreApplication.translate("LoginDialog", u"Dialog", None))
        self.cancelButton.setText(QCoreApplication.translate("LoginDialog", u"Cancel", None))
        self.okButton.setText(QCoreApplication.translate("LoginDialog", u"OK", None))
        self.name.setText(QCoreApplication.translate("LoginDialog", u"Username", None))
        self.pw.setText(QCoreApplication.translate("LoginDialog", u"Password", None))
    # retranslateUi

