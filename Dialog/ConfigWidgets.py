from functools import partial
import ipaddress
import os

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from Dialog.Ui_SerialWidget import Ui_SerialWidget
from Dialog.Ui_EthernetWidget import Ui_EthernetWidget
from Dialog.Ui_Fc7Widget import Ui_Fc7Widget
from Dialog.Ui_PowerSupplyWidget import Ui_PowerSupplyWidget
from Dialog.Ui_ArduinoWidget import Ui_ArduinoWidget


CGREEN = '\033[32m'     #For green prints
CRED   = '\033[91m'     #For red prints
CEND   = '\033[0m'      #End color prints

"""
This file contains all config widgets to change the settings for
FC7
PowerSupply
    PowerSupplyChannels
Arduino
    LEDs

Each widget has its own class
"""

#Parent class of Arduino Power Supply and FC7 widget. For common things like save buttons etc.
class ConfigWidget(QWidget):
    configChanged   = Signal( object )
    commandInput    = Signal( object, object, object )              

    def __init__( self , pDevice ):
        super(ConfigWidget, self).__init__()

    def connectButtons( self ):
        self.ui.checkButton.clicked.connect(partial(self.commandInput.emit, "Check", None, None))
        self.ui.saveButton.clicked.connect(self.saveConfigs)
        self.ui.closeButton.clicked.connect(self.hide)

    def saveConfigs( self ):
        #Emit signal that there was a change. triggers a rewrite of the xml file

        self.configChanged.emit( self.getConfigChanges() )
        self.setButtonColor("Save", "green")
        #This triggers the restart button to turn orange
        self.commandInput.emit("Save", True, None)
        try:
            self.ui.connectionComboBox.setEnabled(True)
        except AttributeError:
            pass

    def setButtonColor(self, pButton, pColor,pText = None):
        if pButton == "Save":
            self.ui.saveButton.setStyleSheet("background-color: " + str(pColor))
        elif pButton == "AddLVChannel":
            self.ui.addLVChannelButton.setStyleSheet("background-color: " + str(pColor))
        elif pButton == "AddHVChannel":
            self.ui.addHVChannelButton.setStyleSheet("background-color: " + str(pColor))

    def setLineColor( self, pLine, pColor, pText = None):
        pLine.setStyleSheet("background-color: " + pColor)

    def configsChanged ( self, pObject= None ):
        self.setButtonColor("Save","orange" )
        #pObject.setStyleSheet("background-color: orange")

    def checkText( self, pText ):
        if ' ' in pText or '_' in pText or '-' in pText or pText == "":
            print("GIPHT:\t" + CRED + "No empty IDS or whitespaces, _ and - in IDs!" + CEND)
            return False
        else:
            return True

class ArduinoWidget(ConfigWidget):
    def __init__( self, pArduino ):
        super(ArduinoWidget, self).__init__( pArduino )
        self.arduino = pArduino
        self.ui = Ui_ArduinoWidget()
        self.ui.setupUi(self)
        self.connectButtons()

        self.kiraCtrl = KIRAControl(pArduino)
        self.ui.configureLedsAndTriggerButton.clicked.connect(self.kiraCtrl.show)
        
        
        self.kiraCtrl.done.connect(partial(self.commandInput.emit,"KIRASettings",self.kiraCtrl.controls,None))
        #self.kiraCtrl.done.connect(self.saveConfigs)


        #Index
        self.ui.indexLabel.setText(str( self.arduino.index ) )
        #ID
        self.ui.idLineEdit.setText(self.arduino.config.get("ID","ArduinoTestBox") )
        self.ui.idLineEdit.textEdited.connect(self.configsChanged)
        self.ui.idLineEdit.textEdited.connect(partial ( self.setLineColor, self.ui.idLineEdit, "orange"))
        #Launch Server
        self.ui.launchServerCheckBox.stateChanged.connect(self.configsChanged)
        self.ui.launchServerCheckBox.stateChanged.connect(self.serverChanged)
        self.ui.externalServerIPPortLineEdit.textEdited.connect(self.configsChanged)

        #Connection Widget
        self.connectionWidget = SerialWidget(pArduino)
        self.connectionWidget.configEdit.connect(partial ( self.setButtonColor, "Save", "orange") )

        self.ui.uploadArduinoSWButton.clicked.connect(partial(self.commandInput.emit, "UploadArduinoSoftware", self.arduino.config.get("Port",""), None))
        self.ui.connectionLayout.addWidget( self.connectionWidget ,0)

        #Sensorsummary as tree widget. Maybe we add arduino LEDs here?
        arduinoHeader = QTreeWidgetItem(["No.","Channel","Value"])
        self.ui.arduinoChannelTreeWidget.setHeaderItem(arduinoHeader)

        tempChannel = QTreeWidgetItem(self.ui.arduinoChannelTreeWidget)
        tempIndexItem = QLabel( "0" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(tempChannel,0,tempIndexItem)
        tempItem = QLabel( "Temperature" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(tempChannel,1,tempItem)
        temperature = QLabel( ) 
        self.ui.arduinoChannelTreeWidget.setItemWidget(tempChannel,2,temperature)

        rhChannel = QTreeWidgetItem(self.ui.arduinoChannelTreeWidget)
        rhIndexItem = QLabel( "1" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(rhChannel,0,rhIndexItem)
        rhItem = QLabel( "Humidity" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(rhChannel,1,rhItem)
        humidity = QLabel( )
        self.ui.arduinoChannelTreeWidget.setItemWidget(rhChannel,2,humidity)

        dpChannel = QTreeWidgetItem(self.ui.arduinoChannelTreeWidget)
        dpIndexItem = QLabel( "2" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(dpChannel,0,dpIndexItem)
        dpItem = QLabel( "Dewpoint" )
        self.ui.arduinoChannelTreeWidget.setItemWidget(dpChannel,1,dpItem)
        dewpoint = QLabel( )
        self.ui.arduinoChannelTreeWidget.setItemWidget(dpChannel,2,dewpoint)


        #Fill eveyrhing with new input
        self.update()

    def update( self ):
        self.ui.version.setText( "SW Version: " + self.arduino.version ) 
        self.ui.launchServerCheckBox.blockSignals(True)
        if str(self.arduino.config.get("LaunchServer", True)) == "True":
            self.ui.launchServerCheckBox.setCheckState(Qt.Checked)
            self.ui.externalServerIPPortLabel.setVisible(False)
            self.ui.externalServerIPPortLineEdit.setVisible(False)
        else:
            self.ui.launchServerCheckBox.setCheckState(Qt.Unchecked)
            self.ui.externalServerIPPortLabel.setVisible(True)
            self.ui.externalServerIPPortLineEdit.setVisible(True)
        self.ui.launchServerCheckBox.blockSignals(False)

        self.ui.externalServerIPPortLineEdit.blockSignals(True)
        self.ui.externalServerIPPortLineEdit.setText(self.arduino.config.get("ExternalIPPort", ""))
        self.ui.externalServerIPPortLineEdit.blockSignals(False)


        try:
            temperatureValue = "{:.2f}".format( float(self.arduino.temp ) )
        except ValueError:
            temperatureValue = "-" 
        self.ui.arduinoChannelTreeWidget.itemWidget ( self.ui.arduinoChannelTreeWidget.topLevelItem( 0 ), 2).setText(temperatureValue   + " °C")

        try:
            humidityValue = "{:.2f}".format( float( self.arduino.rh ) )
        except ValueError:
            humidityValue = "-"

        self.ui.arduinoChannelTreeWidget.itemWidget ( self.ui.arduinoChannelTreeWidget.topLevelItem( 1 ), 2).setText( humidityValue + " %")

        try:
            dewpointValue = "{:.2f}".format( float( self.arduino.dp ) )
        except ValueError:
            dewpointValue = "-"

        self.ui.arduinoChannelTreeWidget.itemWidget ( self.ui.arduinoChannelTreeWidget.topLevelItem( 2 ), 2).setText( dewpointValue + " °C")

    def serverChanged( self ):
        if self.ui.launchServerCheckBox.isChecked():
            self.ui.externalServerIPPortLabel.setVisible(False)
            self.ui.externalServerIPPortLineEdit.setVisible(False)
        else:
            self.ui.externalServerIPPortLabel.setVisible(True)
            self.ui.externalServerIPPortLineEdit.setVisible(True)

    def getConfigChanges( self ):
        configs = {}
        deviceId = self.ui.idLineEdit.text()
        if self.checkText(deviceId):
            configs["ID"] = self.ui.idLineEdit.text()
            self.ui.idLineEdit.setStyleSheet("background-color: green")
        else:
            self.ui.idLineEdit.setStyleSheet("background-color: red")

        configs["LaunchServer"] = self.ui.launchServerCheckBox.isChecked()
        configs["ExternalIPPort"] = self.ui.externalServerIPPortLineEdit.text()
        for key, value in self.connectionWidget.getConfigChanges().items():
            configs[key] = value

        return configs

class KIRAControl(QWidget):
    done = Signal ( )
    def __init__(self, pArduino):
        QWidget.__init__(self)
        self.setGeometry(QRect(100,100,700,200))
        self.setWindowTitle("KIRA Control")
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)

        self.controls = {   "Trigger"   : pArduino.config["Trigger"],
                            "TriggerFrequency" : pArduino.config["TriggerFrequency"],
                            "PulseLength":pArduino.config["PulseLength"]}
        self.leds = []
        self.intensity = []
        grid_layout.addWidget(QLabel("Top LEDs"),0,2)
        grid_layout.addWidget(QLabel("Bottom LEDs"),0,0)
        grid_layout.addWidget(QLabel("Intensity"),0,1)
        grid_layout.addWidget(QLabel("Intensity"),0,3)

        for i in range (16):
            cb = QCheckBox()
            cb.setText(str( i ) )
            self.leds.append(cb)

            if [led for led in pArduino.config["LEDs"] if int ( led["Channel"]) == i  ] [0]["Light"] == "on":
                cb.setCheckState(Qt.Checked)
            else:
                cb.setCheckState(Qt.Unchecked)


            intensityEdit = QLineEdit([led for led in pArduino.config["LEDs"] if int ( led["Channel"]) == i  ] [0]["Intensity"])
            self.intensity.append(intensityEdit)
            if i < 8:
                grid_layout.addWidget(cb,i+1,0)
                grid_layout.addWidget(intensityEdit,i+1,1)
            else:
                grid_layout.addWidget(cb,i-8+1,2)
                grid_layout.addWidget(intensityEdit,i-8+1,3)

        grid_layout.addWidget(QLabel(""),9,0)
        self.triggerFrequency = QLineEdit(str(self.controls["TriggerFrequency"]))
        grid_layout.addWidget(self.triggerFrequency,10,1)
        grid_layout.addWidget(QLabel("Trigger frequency (Hz)"),10,0)

        self.pulseLength = QLineEdit(str(self.controls["PulseLength"]))
        grid_layout.addWidget(self.pulseLength,10,3)
        grid_layout.addWidget(QLabel("Pulse length (ns) "),10,2)

        self.triggerState = QCheckBox("Trigger On/Off")
        grid_layout.addWidget(self.triggerState,11,1)
        if self.controls["Trigger"] == "on":
            self.triggerState.setCheckState(Qt.Checked)
        else:
            self.triggerState.setCheckState(Qt.Unchecked)

        self.applyButton = QPushButton("Apply")
        grid_layout.addWidget(self.applyButton,11,3)

        self.applyButton.clicked.connect(self.emitChanges)

        self.cancelButton = QPushButton("Cancel")
        grid_layout.addWidget(self.cancelButton,11,0)
        self.cancelButton.clicked.connect(self.close)

    def emitChanges( self ):
        self.controls["Trigger"] =   "on" if self.triggerState.isChecked() else "off"
        self.controls["TriggerFrequency"] = self.triggerFrequency.text()
        self.controls["PulseLength"] = self.pulseLength.text()
        self.controls["LEDs"] = []
        for i in range (16):
            led = {}
            led["Channel"]      = str(i)
            led["Intensity"]    = self.intensity[i].text()
            led["Light"]        = "on" if self.leds[i].isChecked() else "off"
            self.controls["LEDs"].append(led)

        self.done.emit()
        self.close()

class PowerSupplyWidget(ConfigWidget):
    def changeConnectionType( self ):
        if self.ui.connectionLayout.itemAt(0) is not None:
            widget = self.ui.connectionLayout.itemAt(0).widget()
            self.ui.connectionLayout.removeWidget(widget)
            widget.deleteLater()
            widget = None

        if self.powerSupply.config.get("Connection", "Ethernet") in ["Serial"]:
            self.connectionWidget = SerialWidget(self.powerSupply)
        elif self.powerSupply.config.get("Connection", "Ethernet") in ["Ethernet"]:
            self.connectionWidget = EthernetWidget(self.powerSupply)

        self.ui.connectionLayout.addWidget( self.connectionWidget ,0)

    def connectionChanged(self, pConnectionType):
        self.configChanged.emit({"Connection": pConnectionType})
        self.changeConnectionType()

    def __init__( self, pPowerSupply ):
        super(PowerSupplyWidget, self).__init__( pPowerSupply )
        self.powerSupply = pPowerSupply
        self.powerSupply.configUpdate.connect(self.updateConfigs)
        self.ui = Ui_PowerSupplyWidget()
        self.ui.setupUi(self)
        self.connectButtons()
        self.ui.addLVChannelButton.clicked.connect( partial( self.tryToAddNewChannel, "LV" ) )
        self.ui.addLVChannelButton.clicked.connect( self.configsChanged )
        self.ui.addHVChannelButton.clicked.connect( partial( self.tryToAddNewChannel, "HV" ) )
        self.ui.addHVChannelButton.clicked.connect( self.configsChanged )
        self.ui.removeChannelButton.clicked.connect( self.removeChannel )
        self.ui.removeChannelButton.clicked.connect( self.configsChanged )

        self.listedLVChannels = 0
        self.listedHVChannels = 0


        #Index
        self.ui.indexLabel.setText(str( self.powerSupply.index ) )
        #ID
        self.ui.idLineEdit.setText(self.powerSupply.config.get("ID",self.powerSupply.config["Model"] + "TestBox") )
        self.ui.idLineEdit.textEdited.connect(partial ( self.setLineColor, self.ui.idLineEdit, "orange"))
        #Launch Server
        self.ui.launchServerCheckBox.stateChanged.connect(self.configsChanged)
        self.ui.launchServerCheckBox.stateChanged.connect(self.serverChanged)
        self.ui.externalServerIPPortLineEdit.textEdited.connect(self.configsChanged)
        #Model
        self.ui.modelComboBox.addItems(self.powerSupply.modelDictionary.keys())
        index = self.ui.modelComboBox.findText(self.powerSupply.config.get("Model",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.modelComboBox.setCurrentIndex(index)
        self.ui.modelComboBox.currentTextChanged.connect(self.configsChanged)
        self.ui.modelComboBox.currentTextChanged.connect(self.modelChanged)

        #Series
        self.ui.seriesComboBox.addItems(self.powerSupply.modelDictionary[self.powerSupply.config.get("Model")]["Series"])
        index = self.ui.seriesComboBox.findText(self.powerSupply.config.get("Series",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.seriesComboBox.setCurrentIndex(index)
        self.ui.seriesComboBox.currentTextChanged.connect(self.configsChanged)

        #Connection Widget
        self.connectionWidget = None
        self.connectionChanged( self.ui.connectionComboBox.currentText())
        self.connectionWidget.configEdit.connect(partial ( self.setButtonColor, "Save", "orange") )
        self.ui.connectionComboBox.currentTextChanged.connect(self.connectionChanged)


        self.ui.channelTreeWidget.blockSignals(True)
        if pPowerSupply.isHVLV() == "LV":
            channelHeader = QTreeWidgetItem(["No.","HVLV","Ch","ID","Umin","Umax","Uset (V)","U (V)","I (A)","On"])
        elif pPowerSupply.isHVLV() == "HV":
            channelHeader = QTreeWidgetItem(["No.","HVLV","Ch","ID","Umin","Umax","Uset (V)","U (V)","I (nA)","On"])
        elif pPowerSupply.isHVLV() == "HVLV":
            channelHeader = QTreeWidgetItem(["No.","HVLV","Ch","ID","Umin","Umax","Uset (V)","U (V)","I (nA)","On"])
        self.ui.channelTreeWidget.setHeaderItem(channelHeader)

        for channel in self.powerSupply.channels:
            self.addChannel(channel)
            
        for column in range(9):
            self.ui.channelTreeWidget.resizeColumnToContents( column )
        header = self.ui.channelTreeWidget.header()
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(7, QHeaderView.Stretch)

        if self.ui.modelComboBox.currentText() == "CAEN":
            self.ui.hvSlotLabel.show()
            self.ui.lvSlotLabel.show()
            self.ui.hvSlotComboBox.show()
            self.ui.lvSlotComboBox.show()
            hvSlotIndex = int(self.ui.hvSlotComboBox.findText(self.powerSupply.config.get("HVSlot","0"), Qt.MatchFixedString) )
            if hvSlotIndex >= 0:
                self.ui.hvSlotComboBox.setCurrentIndex(hvSlotIndex)
            self.ui.hvSlotComboBox.currentTextChanged.connect(self.configsChanged)
            lvSlotIndex = int(self.ui.lvSlotComboBox.findText(self.powerSupply.config.get("LVSlot","0"), Qt.MatchFixedString) )
            if lvSlotIndex >= 0:
                self.ui.lvSlotComboBox.setCurrentIndex(lvSlotIndex)
            self.ui.lvSlotComboBox.currentTextChanged.connect(self.configsChanged)
        else:
            self.ui.hvSlotLabel.hide()
            self.ui.lvSlotLabel.hide()
            self.ui.hvSlotComboBox.hide()
            self.ui.lvSlotComboBox.hide()

        self.update()
        self.saveConfigs()
        self.updateConfigs()

    def modelChanged( self ):
        #Clear combobox and load series from model dictionary
        self.ui.seriesComboBox.clear()
        self.ui.seriesComboBox.addItems(self.powerSupply.modelDictionary[self.ui.modelComboBox.currentText()]["Series"])
        index = self.ui.seriesComboBox.findText(self.powerSupply.config.get("Series",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.seriesComboBox.setCurrentIndex(index)
        
        #remove all channels from the tree
        for i in range (self.ui.channelTreeWidget.topLevelItemCount()):
            self.removeChannel()

        self.ui.addLVChannelButton.setEnabled(False)
        self.ui.addHVChannelButton.setEnabled(False)
        self.setButtonColor("AddLVChannel", None)
        self.setButtonColor("AddHVChannel", None)
        self.ui.connectionComboBox.setDisabled(True)

        #Load default port
        if self.powerSupply.config["Connection"] == "Ethernet":
            self.connectionWidget.ui.portLineEdit.setText( str( self.powerSupply.modelDictionary[self.ui.modelComboBox.currentText()]["DefaultPort"] ) )

        if self.ui.modelComboBox.currentText() == "CAEN":
            self.ui.hvSlotLabel.show()
            self.ui.lvSlotLabel.show()
            self.ui.hvSlotComboBox.show()
            self.ui.lvSlotComboBox.show()
        else:
            self.ui.hvSlotLabel.hide()
            self.ui.lvSlotLabel.hide()
            self.ui.hvSlotComboBox.hide()
            self.ui.lvSlotComboBox.hide()

    def serverChanged( self ):
        if self.ui.launchServerCheckBox.isChecked():
            self.ui.externalServerIPPortLabel.setVisible(False)
            self.ui.externalServerIPPortLineEdit.setVisible(False)
        else:
            self.ui.externalServerIPPortLabel.setVisible(True)
            self.ui.externalServerIPPortLineEdit.setVisible(True)

    def tryToAddNewChannel( self, pHVLV ):
        if pHVLV == "LV":
            listed = self.listedLVChannels
        if pHVLV == "HV":
            listed = self.listedHVChannels

        newChannel = self.powerSupply.getNewChannel(listed, pHVLV)
        if newChannel is not None:
            self.addChannel( newChannel )


    def removeChannel( self ):
        itemRemoved = self.ui.channelTreeWidget.takeTopLevelItem(self.ui.channelTreeWidget.topLevelItemCount()-1)
        if itemRemoved is not None:
            if itemRemoved.text(1) == "LV":
                print("minusLV")
                if self.listedLVChannels > 0:
                    self.listedLVChannels -= 1
            if itemRemoved.text(1) == "HV":
                print("minusHV")
                if self.listedHVChannels > 0:
                    self.listedHVChannels -= 1
        self.updateConfigs()

    def addChannel( self, channel):

        self.setButtonColor("AddChannel", None)
        
        newChannel = QTreeWidgetItem(self.ui.channelTreeWidget)
        index = self.ui.channelTreeWidget.topLevelItemCount()-1
        newChannel.setText(0, str(index+1))        
        newChannel.setText(1, channel.config["HVLV"])

        channelComboBox = QComboBox()
        channelComboBox.addItems(self.powerSupply.modelDictionary[self.powerSupply.config["Model"]]["Channels"][channel.config["HVLV"]]  )
        self.ui.channelTreeWidget.setItemWidget(newChannel,2,channelComboBox)

        channelComboBox.findText(channel.config.get("Channel",""), Qt.MatchFixedString)
        if index >= 0:
            channelComboBox.setCurrentIndex(index)
        channelComboBox.currentTextChanged.connect(self.configsChanged)

        idItem = QLineEdit(channel.config["ID"])
        idItem.setMaximumSize(QSize(60, 31))
        idItem.textEdited.connect(self.configsChanged)
        idItem.textEdited.connect(partial (self.setCellColor, index, 3, "orange" ) )
        self.ui.channelTreeWidget.setItemWidget(newChannel,3,idItem)

        uMinItem = QLineEdit(str(channel.config["UNVThr"]))
        uMinItem.setMaximumSize(QSize(45, 31))
        self.ui.channelTreeWidget.setItemWidget(newChannel,4,uMinItem)      
        uMinItem.setDisabled(True)

        uMaxItem = QLineEdit(str(channel.config["OVVThr"]))
        uMaxItem.setMaximumSize(QSize(45, 31))
        self.ui.channelTreeWidget.setItemWidget(newChannel,5,uMaxItem)      
        uMaxItem.setDisabled(True)

        vSetItem = QLineEdit()
        vSetItem.setMaximumSize(QSize(70, 31))
        self.ui.channelTreeWidget.setItemWidget(newChannel,6,vSetItem)
        vSetItem.returnPressed.connect(partial (self.newCommand, index, "v_set" ) ) 
        vSetItem.textEdited.connect(partial (self.setCellColor, index, 6, "orange" ) ) 

        voltageItem = QLabel()
        self.ui.channelTreeWidget.setItemWidget(newChannel,7,voltageItem)

        currentItem = QLabel( )
        self.ui.channelTreeWidget.setItemWidget(newChannel,8,currentItem)

        activeCheckBox = QCheckBox( )
        self.ui.channelTreeWidget.setItemWidget(newChannel,9,activeCheckBox)   
        activeCheckBox.stateChanged.connect( partial ( self.turnOnOff, index) )

        if channel.config["HVLV"] == "LV":
            self.listedLVChannels += 1
        if channel.config["HVLV"] == "HV":
            self.listedHVChannels += 1
        
        self.updateConfigs()

    def updateConfigs( self ):
        if self.powerSupply.isHVLV() == "LV":
            self.ui.addLVChannelButton.setEnabled(True)
            self.ui.addHVChannelButton.setEnabled(False)
            if self.listedLVChannels == 0:
                self.setButtonColor("AddLVChannel", "orange")
                self.setButtonColor("AddHVChannel", None)
        elif self.powerSupply.isHVLV() == "HV":
            if self.listedHVChannels == 0:
                self.setButtonColor("AddLVChannel", None)
                self.setButtonColor("AddHVChannel", "orange")
            self.ui.addLVChannelButton.setEnabled(False)
            self.ui.addHVChannelButton.setEnabled(True)
        elif self.powerSupply.isHVLV() == "HVLV":
            if self.listedLVChannels + self.listedHVChannels == 0:
                self.setButtonColor("AddLVChannel", "orange")
                self.setButtonColor("AddHVChannel", "orange")
            self.ui.addLVChannelButton.setEnabled(True)
            self.ui.addHVChannelButton.setEnabled(True)
        if self.listedLVChannels + self.listedHVChannels != 0:
            self.setButtonColor("AddLVChannel", None)
            self.setButtonColor("AddHVChannel", None)

    def update( self ):
        self.ui.launchServerCheckBox.blockSignals(True)
        if str(self.powerSupply.config.get("LaunchServer", True)) == "True":
            self.ui.launchServerCheckBox.setCheckState(Qt.Checked)
        else:
            self.ui.launchServerCheckBox.setCheckState(Qt.Unchecked)
        self.ui.launchServerCheckBox.blockSignals(False)
        self.serverChanged()
        self.ui.externalServerIPPortLineEdit.blockSignals(True)
        self.ui.externalServerIPPortLineEdit.setText(self.powerSupply.config.get("ExternalIPPort", ""))
        self.ui.externalServerIPPortLineEdit.blockSignals(False)

        for channel in self.powerSupply.channels:
            index = self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 2).findText(channel.config.get("Channel",""), Qt.MatchFixedString)
            if index >= 0:
                self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 2).setCurrentIndex(index)


            self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 3).setText(channel.config.get("ID",""))
            self.setCellColor(channel.index, 3, "white")

            #v_min = channel.config["UNVThr"]
            #v_max = channel.config["OVVThr"]
            #print("v_min")
            #print(v_min)
            #self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 4).setText(str(v_min))
            #self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 5).setText(str(v_max))


            v_set   = self.formatValue( channel.status["v_set"] )
            v_meas  = self.formatValue( channel.status["Voltage"] )

            if self.powerSupply.isHVLV() == "LV":
                i_meas  = self.formatValue( channel.status["Current"] )
            else:
                try:
                    i_meas = str( "{:.2f}".format(float ( channel.status["Current"] ) * 10**9 ) )
                except:
                    i_meas = "-"


            self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 6).setPlaceholderText(v_set)

            self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 7).setText(v_meas + "   " )
            self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 8).setText(i_meas + "   " )


            box = self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( channel.index), 9)
            box.blockSignals(True)
            if channel.status["IsOn"]== "1":
                box.setCheckState(Qt.Checked)
                box.setStyleSheet("QCheckBox::indicator""{background-color: green;""}")
            elif channel.status["IsOn"]== "0":
                box.setCheckState(Qt.Unchecked)
                box.setStyleSheet("QCheckBox::indicator""{background-color: red;""}")
            else:
                box.setCheckState(Qt.Unchecked)
                box.setStyleSheet("QCheckBox::indicator""{background-color: orange;""}")
            box.blockSignals(False)



    def turnOnOff(self, pChannelIndex, pState):
        self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( pChannelIndex), 7).setStyleSheet("QCheckBox::indicator""{background-color: orange;""}")
        self.commandInput.emit("EnableDisable", None, pChannelIndex)
            
    def setCellColor( self, pChannelIndex, pColumn, pColor, pText = None):
        self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( pChannelIndex), pColumn).setStyleSheet("background-color: " + pColor)

    def newCommand(self, pChannelIndex, pSetting, pValue = None):
        settings = ["v_set"]
        try:
            value = float(self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( pChannelIndex), settings.index(pSetting) + 6).text())
            self.commandInput.emit(pSetting,value,pChannelIndex) #Send command set v or i
            self.setCellColor(pChannelIndex, settings.index(pSetting) + 6, "green")
            self.ui.channelTreeWidget.itemWidget ( self.ui.channelTreeWidget.topLevelItem( pChannelIndex), settings.index(pSetting) + 6).setModified(False)
        except ValueError:
            print("GIPHT:\tPlease type in a decimal number!")
            self.setCellColor(pChannelIndex, settings.index(pSetting) + 6, "red")

    def formatValue( self, pValue):
        try:
            return "{:.2f}".format( float ( pValue ) )
        except ValueError:
            return "-"

    def getConfigChanges( self ):
        configs = {}

        #Check if device name is good (not _ or -)
        deviceId = self.ui.idLineEdit.text()
        if self.checkText(deviceId):
            configs["ID"] = self.ui.idLineEdit.text()
            self.ui.idLineEdit.setStyleSheet("background-color: green")
        else:
            self.ui.idLineEdit.setStyleSheet("background-color: red")

        #Also check if the channel naming is fine
        configs["Channels"] = []
        for index in range( self.ui.channelTreeWidget.topLevelItemCount() ):
            
            ch = {}
            ch["HVLV"] = self.ui.channelTreeWidget.topLevelItem( index ).text(1)
            ch["Channel"] = self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),2).currentText()
            channelId = self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),3).text()

            if self.checkText(channelId):
                self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),3).setStyleSheet("background-color: green")
                ch["ID"] = channelId
                configs["Channels"].append(ch)
            else:
                #ChannelId not okay. Lets take the old one or create another one
                self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),3).setStyleSheet("background-color: red")

            ch["UNVThr"] = self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),4).text()
            ch["OVVThr"] = self.ui.channelTreeWidget.itemWidget(self.ui.channelTreeWidget.topLevelItem( index ),5).text()

        configs["Model"] = self.ui.modelComboBox.currentText()
        configs["Series"] = self.ui.seriesComboBox.currentText()
        configs["HVSlot"] = self.ui.hvSlotComboBox.currentText()
        configs["LVSlot"] = self.ui.lvSlotComboBox.currentText()

        configs["LaunchServer"] = str(self.ui.launchServerCheckBox.isChecked())
        configs["ExternalIPPort"] = self.ui.externalServerIPPortLineEdit.text()


        for key, value in self.connectionWidget.getConfigChanges().items():
            configs[key] = value
        return configs

class Fc7Widget(ConfigWidget):

    def __init__(self, pDevice, pModelList = None):
        super(Fc7Widget, self).__init__( pDevice )
        self.fc7 = pDevice
        self.ui = Ui_Fc7Widget()
        self.ui.setupUi(self)
        self.connectButtons()
        #Index
        self.ui.indexLabel.setText(str( self.fc7.index ) )
        #ID
        self.ui.idLineEdit.setText(self.fc7.config.get("ID","FC7TestBox") )
        self.ui.idLineEdit.textEdited.connect(self.configsChanged)

        #IpAddresses
        self.ui.searchFc7sButton.clicked.connect( partial( self.fillFc7IpAddressList, pDevice ) )
        self.ui.ipAddressComboBox.currentTextChanged.connect(self.configsChanged)
        #IPLine
        self.ui.ipLineEdit.textEdited.connect(self.configsChanged)
        #Firmware
        self.ui.firmwareComboBox.currentIndexChanged.connect(self.switchFirmware)
        self.ui.load2SFEC5FWButton.clicked.connect( partial( self.checkAndLoadFirmware, "2S_FEC5") )
        self.ui.load2SFEC12FWButton.clicked.connect( partial( self.checkAndLoadFirmware, "2S_FEC12") )
        self.ui.loadPS5GFWButton.clicked.connect( partial( self.checkAndLoadFirmware, "PS_5G") )
        self.ui.loadPS10GFWButton.clicked.connect( partial( self.checkAndLoadFirmware, "PS_10G") )

        self.ui.saveButton.clicked.connect( partial(self.setButtonColor, "Save", "green" ))

        self.update()

    def update( self ):
        #Label
        self.ui.indexLabel.setText(str( self.fc7.index ) )
        #ID
        self.ui.idLineEdit.blockSignals(True)
        self.ui.idLineEdit.setText(self.fc7.config.get("ID","") )
        self.ui.idLineEdit.blockSignals(False)
        #IPs
        self.ui.ipAddressComboBox.blockSignals(True)
        self.ui.ipAddressComboBox.addItems(self.fc7.availableFc7s)
        index = self.ui.ipAddressComboBox.findText(self.fc7.config.get("IPAddress",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.ipAddressComboBox.setCurrentIndex(index)
        self.ui.ipAddressComboBox.blockSignals(False)
        #IPLine
        self.ui.ipLineEdit.blockSignals(True)
        self.ui.ipLineEdit.setText(self.fc7.config.get("IPAddress",""))
        self.ui.ipLineEdit.blockSignals(False)
        #Firmware 
        self.ui.firmwareLabel.setText(self.fc7.firmwareName +"\n"+ str(self.fc7.nHybrids) + " Hybrids " + str(self.fc7.nChips) + " " + self.fc7.chipType +" (" + self.fc7.timeStamp + ")" )
        self.ui.firmwareComboBox.blockSignals(True)
        self.ui.firmwareComboBox.clear()
        self.ui.firmwareComboBox.addItems(self.fc7.firmwareList)
        index = self.ui.firmwareComboBox.findText(self.fc7.firmwareName, Qt.MatchFixedString)
        if index >= 0:
            self.ui.firmwareComboBox.setCurrentIndex(index)
        self.ui.firmwareComboBox.blockSignals(False)

    def checkIp (self, pIp):
        try:
            ipaddress.IPv4Network(pIp)
            return True
        except ValueError:
            return False

    def getConfigChanges( self ):
        configs = {}
        configs["ID"] = self.ui.idLineEdit.text()

        if self.ui.ipLineEdit.text() == "":
            ip = self.ui.ipAddressComboBox.currentText()
        else:
            if self.ui.ipAddressComboBox.currentText() == "":
                ip = self.ui.ipLineEdit.text()
            else:
                ip = self.ui.ipAddressComboBox.currentText()
                self.ui.ipLineEdit.setText(ip)

        if self.checkIp(ip):
            configs["IPAddress"] = ip
            self.ui.ipLineEdit.setStyleSheet("background-color: green")
        else:
            self.ui.ipLineEdit.setStyleSheet("background-color: red")

        return configs

    def fillFc7IpAddressList( self, pDevice ):
        ipList = pDevice.findFC7sInNetwork()
        self.ui.ipAddressComboBox.clear()
        self.ui.ipAddressComboBox.addItems(ipList)

    #Switch FW is chosen from the dropdown menu, thus the FW is definetly available
    def switchFirmware(self):
        self.commandInput.emit("SwitchFW", self.ui.firmwareComboBox.currentText(), None)

    def checkAndLoadFirmware( self, pType ):
        fw = [fw for fw in os.listdir(os.getcwd() + "/Bitstreams/") if pType in fw][0]

        if fw in self.fc7.firmwareList:
            self.commandInput.emit("SwitchFW", fw, None)
        else:
            self.commandInput.emit("UploadFW", fw, None)



class SerialWidget(QWidget):
    baudRates = ["50","110","150","300","1200","2400","4800","9600","19200","38400","57600","115200","230400","460800","500000"]
    terminator = ["CR","LF","CRLF","LFCR"]
    configEdit = Signal( object )
    def __init__( self, pDevice ):
        super(SerialWidget, self).__init__()
        self.ui = Ui_SerialWidget()
        self.ui.setupUi(self)

        self.device = pDevice

        #SerialPorts
        self.ui.portComboBox.currentTextChanged.connect(self.configEdit.emit)

        #Baudrate
        self.ui.baudRateComboBox.addItems(self.baudRates)
        self.ui.baudRateComboBox.currentTextChanged.connect(self.configEdit.emit)

        #Terminator
        self.ui.terminatorComboBox.addItems(self.terminator)
        self.ui.terminatorComboBox.currentTextChanged.connect(self.configEdit.emit)

        #Suffix
        self.ui.suffixComboBox.addItems(self.terminator)
        self.ui.suffixComboBox.currentTextChanged.connect(self.configEdit.emit)

        #Parity
        self.ui.noneParityCheckBox.toggled.connect(self.configEdit.emit)
        self.ui.noneParityCheckBox.toggled.connect(self.ui.oddParityRadioButton.setDisabled)
        self.ui.noneParityCheckBox.toggled.connect(self.ui.evenParityRadioButton.setDisabled)

        self.ui.evenParityRadioButton.toggled.connect(self.configEdit.emit)
        self.ui.oddParityRadioButton.toggled.connect(self.configEdit.emit)

        #FlowControl
        self.ui.flowControlCheckBox.toggled.connect(self.configEdit.emit)

        #RemoveEcho
        self.ui.removeEchoCheckBox.toggled.connect(self.configEdit.emit)

        #Timeout
        self.ui.timeoutLineEdit.textEdited.connect(self.configEdit.emit)
        self.ui.timeoutLineEdit.textEdited.connect(partial ( self.setLineColor, self.ui.timeoutLineEdit, "orange"))

        self.ui.refreshPortsButton.clicked.connect( partial ( self.refreshPorts , self.device) )
        self.update()

    def update ( self ):
        #Signals must be blocked when overwritten

        #Serial Ports
        self.refreshPorts(self.device)
        #Baudrate
        self.ui.baudRateComboBox.blockSignals(True)
        index = self.ui.baudRateComboBox.findText(self.device.config.get("BaudRate",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.baudRateComboBox.setCurrentIndex(index)
        else:
            self.ui.baudRateComboBox.setCurrentIndex(7)
        self.ui.baudRateComboBox.blockSignals(False)
        #Terminator
        self.ui.terminatorComboBox.blockSignals(True)
        index = self.ui.terminatorComboBox.findText(self.device.config.get("Terminator",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.terminatorComboBox.setCurrentIndex(index)
        self.ui.terminatorComboBox.blockSignals(False)
        #Suffix
        self.ui.suffixComboBox.blockSignals(True)
        index = self.ui.suffixComboBox.findText(self.device.config.get("Suffix",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.suffixComboBox.setCurrentIndex(index)
        self.ui.suffixComboBox.blockSignals(False)
        #Parity
        self.ui.noneParityCheckBox.blockSignals(True)
        self.ui.evenParityRadioButton.blockSignals(True)
        self.ui.oddParityRadioButton.blockSignals(True)
        if self.device.config.get("Parity","") == "None":
            self.ui.noneParityCheckBox.setChecked(True)
            self.ui.oddParityRadioButton.setDisabled(True)
            self.ui.evenParityRadioButton.setDisabled(True)
        elif self.device.config.get("Parity","") == "Even":
            self.ui.evenParityRadioButton.setChecked(True)
        elif self.device.config.get("Parity","") == "Odd":
            self.ui.oddParityRadioButton.setChecked(True)
        else:
            self.ui.noneParityCheckBox.setChecked(True)
            self.ui.oddParityRadioButton.setDisabled(True)
            self.ui.evenParityRadioButton.setDisabled(True)

        self.ui.noneParityCheckBox.blockSignals(False)
        self.ui.evenParityRadioButton.blockSignals(False)
        self.ui.oddParityRadioButton.blockSignals(False)
        #FlowControl
        self.ui.flowControlCheckBox.blockSignals(True)
        if self.device.config.get("FlowControl","") == "True":
            self.ui.flowControlCheckBox.setChecked(True)
        self.ui.flowControlCheckBox.blockSignals(False)
        #RemoveEcho
        self.ui.removeEchoCheckBox.blockSignals(True)
        if self.device.config.get("RemoveEcho","") == "True":
            self.ui.removeEchoCheckBox.setChecked(True)
        self.ui.removeEchoCheckBox.blockSignals(False)
        #Timeout
        self.ui.timeoutLineEdit.blockSignals(True)
        self.ui.timeoutLineEdit.setText(self.device.config.get("Timeout","1"))
        self.ui.timeoutLineEdit.blockSignals(False)

    def setLineColor( self, pLine, pColor, pText = None):
        pLine.setStyleSheet("background-color: " + pColor)

    def refreshPorts( self, pDevice ):
        self.ui.portComboBox.clear()
        self.ui.portComboBox.addItems(pDevice.getSerialPorts())
        index = self.ui.portComboBox.findText(pDevice.config.get("Port",""), Qt.MatchFixedString)
        if index >= 0:
            self.ui.portComboBox.setCurrentIndex(index)
        else:
            self.ui.portComboBox.setStyleSheet("color: red")

    def getConfigChanges( self ):
        configs = {}
        configs["Port"] = self.ui.portComboBox.currentText()
        configs["BaudRate"] = self.ui.baudRateComboBox.currentText()
        configs["Terminator"] = self.ui.terminatorComboBox.currentText()
        configs["Suffix"] = self.ui.suffixComboBox.currentText()
        if self.ui.noneParityCheckBox.isChecked():
            configs["Parity"] = "None"
        else:
            if self.ui.evenParityRadioButton.isChecked():
                configs["Parity"] = "Even"
            if self.ui.oddParityRadioButton.isChecked():
                configs["Parity"] = "Odd"
        if self.ui.flowControlCheckBox.isChecked():
            configs["FlowControl"] = "True"
        else:
            configs["FlowControl"] = "False"
        if self.ui.removeEchoCheckBox.isChecked():
            configs["RemoveEcho"] = "True"
        else:
            configs["RemoveEcho"] = "False"

        try:
            timeout = int(self.ui.timeoutLineEdit.text() )
            self.setLineColor(self.ui.timeoutLineEdit, "green")
        except ValueError:
            print("GIPHT:\tPlease type in a integeer number!")
            self.setLineColor(self.ui.timeoutLineEdit, "red")
            timeout = 0
        configs["Timeout"] = str(timeout)

        #Extend with parity etc
        return configs


class EthernetWidget(QWidget):
    configEdit = Signal()
    def __init__(self, pDevice, pModelList = None):
        super(EthernetWidget, self).__init__()
        self.ui = Ui_EthernetWidget()
        self.ui.setupUi(self)
        self.device = pDevice


        #Ip Address Port
        self.ui.ipAddressLineEdit.textEdited.connect(self.configEdit.emit)
        #Port
        self.ui.portLineEdit.textEdited.connect(self.configEdit.emit)
        self.ui.portLineEdit.setText( str ( self.device.modelDictionary [ self.device.config [ "Model" ] ] ["DefaultPort"] ) )
        self.update()

    def update( self ):
        #Ip Address Port
        self.ui.ipAddressLineEdit.blockSignals(True)
        self.ui.ipAddressLineEdit.setText(self.device.config.get("IPAddress","") )
        self.ui.ipAddressLineEdit.blockSignals(False)

        #Port
        self.ui.portLineEdit.blockSignals(True)
        self.ui.portLineEdit.setText(self.device.config.get("Port","5025") )
        self.ui.portLineEdit.blockSignals(False)

        #Timeout
        self.ui.timeoutLineEdit.blockSignals(True)
        self.ui.timeoutLineEdit.setText(self.device.config.get("Timeout","1") )
        self.ui.timeoutLineEdit.blockSignals(False)


    def getConfigChanges( self ):
        configs = {}
        configs["Port"] = self.ui.portLineEdit.text()
        configs["IPAddress"] = self.ui.ipAddressLineEdit.text()
        configs["Timeout"] = self.ui.timeoutLineEdit.text()
        return configs


