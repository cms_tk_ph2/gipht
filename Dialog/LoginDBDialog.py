#------------------------------------------------
#++++++++++++++++DB Login.py+++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      23.02.2021
#------------------------------------------------

from Dialog.Ui_LoginDB import Ui_LoginDialog
from PySide2.QtWidgets import QDialog, QLineEdit
from PySide2.QtCore import Signal


class LoginDBDialog(QDialog):
    sendCredentials = Signal ( object )

    def __init__( self ):
        super(LoginDBDialog, self).__init__()

        self.ui = Ui_LoginDialog()
        self.ui.setupUi(self)
        self.setWindowTitle("DB Login")
        self.ui.pwLineEdit.setEchoMode(QLineEdit.Password)
        self.ui.cancelButton.clicked.connect(self.closeClicked)
        self.ui.okButton.clicked.connect(self.checkAndSendCredentials)

    #Called when cancel button is clicked, closes the dialog
    def closeClicked( self ):
        self.close()

    def checkAndSendCredentials( self ):
        self.sendCredentials.emit([self.ui.nameLineEdit.text(), self.ui.pwLineEdit.text()])
        self.close()

