#!/bin/bash
#Run to build all the custom Python classes for the interfaces from the .ui files
pyside2-uic -o Dialog/Ui_ConfigureSlotsDialog.py Dialog/ConfigureSlotsDialog.ui
pyside2-uic -o Dialog/Ui_SlotWidget.py Dialog/SlotWidget.ui
pyside2-uic -o Dialog/Ui_SlotStatusWidget.py Dialog/SlotStatusWidget.ui
pyside2-uic -o Dialog/Ui_Monitor.py Dialog/Monitor.ui
pyside2-uic -o Dialog/Ui_PowerSupplyWidget.py Dialog/PowerSupplyWidget.ui
pyside2-uic -o Dialog/Ui_SerialWidget.py Dialog/SerialWidget.ui
pyside2-uic -o Dialog/Ui_EthernetWidget.py Dialog/EthernetWidget.ui
pyside2-uic -o Dialog/Ui_Fc7Widget.py Dialog/Fc7Widget.ui
pyside2-uic -o Dialog/Ui_ArduinoWidget.py Dialog/ArduinoWidget.ui
pyside2-uic -o Dialog/Ui_LoginDB.py Dialog/LoginDB.ui
pyside2-uic -o Ui_MainWindow.py MainWindow.ui
