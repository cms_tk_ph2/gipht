from Dialog.Ui_SlotStatusWidget import Ui_SlotStatusWidget
from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Qt, Signal, QSize
from PySide2.QtGui import QIcon

from functools import partial

class SlotStatusWidget(QWidget):
    moduleInput = Signal(  )
    def __init__(self, pSlot):
        super(SlotStatusWidget, self).__init__()
        self.ui = Ui_SlotStatusWidget()
        self.ui.setupUi(self)

        self.slot = pSlot
        self.ui.infoButton.setDisabled(True)
        self.ui.moduleIdLineEdit.setPlaceholderText("Module ID")
        self.ui.commentLineEdit.setPlaceholderText("SEH / ROH ID")

        self.ui.moduleIdLineEdit.textEdited.connect (self.moduleIdEdit)
        self.ui.moduleIdLineEdit.returnPressed.connect (self.moduleInput.emit)
        self.ui.previousIdButton.clicked.connect(self.reloadPreviousModuleId)
        self.ui.previousIdButton.clicked.connect( self.moduleInput.emit )

        self.ui.encapsulatedCheckBox.stateChanged.connect( partial ( self.updateModule, "Encapsulation" ) )
        self.ui.skeletonCheckBox.stateChanged.connect( partial ( self.updateModule, "Skeleton" ) )
        self.ui.commentLineEdit.returnPressed.connect( partial ( self.updateModule, "Comment" ) )


        icon = QIcon()
        icon.addFile(u"./Utils/reload.png", QSize(), QIcon.Normal, QIcon.Off)
        self.ui.previousIdButton.setIcon(icon)


        self.ui.lvVoltage.setText("LV_V")
        self.ui.lvCurrent.setText("LV_I")
        self.ui.hvVoltage.setText("HV_V")
        self.ui.hvCurrent.setText("HV_I")
        self.ui.rh.setText("RH")
        self.ui.t.setText("T")

        #self.ui.slotStatusLayout.addStretch()

        self.ui.slotCheckBox.setText("Slot " + str(self.slot.index))
        self.ui.slotCheckBox.setCheckState(Qt.Checked)


    def reloadPreviousModuleId( self ):
        self.ui.moduleIdLineEdit.setText(self.slot.lastModuleId)

    def moduleIdEdit( self , pText):
        self.ui.moduleIdLineEdit.setStyleSheet("background-color: orange")
        self.slot.lastModuleId = pText

    def updateModule( self, pType, pValue = ""):
        if pType == "Encapsulation":
            self.slot.module.encapsulated = pValue
        elif pType == "Skeleton":
            self.slot.module.skeleton = pValue
        elif pType == "Comment":
            self.slot.comment = self.ui.commentLineEdit.text()
            self.ui.commentLineEdit.setStyleSheet("background-color: green")

    def clearSlot( self ):
        self.ui.commentLineEdit.setStyleSheet("")
        self.ui.moduleIdLineEdit.setStyleSheet("")
        self.ui.commentLineEdit.setText("")
        self.ui.moduleIdLineEdit.setText("")

    def updateStatus ( self ):
        if self.slot.lvPowerSupplyChannel is not None:
            self.ui.lvVoltage.setText( "{:.2f}".format( float ( self.slot.lvPowerSupplyChannel.status["Voltage"] ) ) + "V" )
            self.ui.lvCurrent.setText( "{:.2f}".format( float ( self.slot.lvPowerSupplyChannel.status["Current"] ) ) + "A" )
            if self.slot.lvPowerSupplyChannel.status["IsOn"] == "1":
                self.ui.lvCurrent.setStyleSheet("color: green")
                self.ui.lvVoltage.setStyleSheet("color: green")
            if self.slot.lvPowerSupplyChannel.status["IsOn"] == "0":
                self.ui.lvCurrent.setStyleSheet("color: red")
                self.ui.lvVoltage.setStyleSheet("color: red")
            if self.slot.lvPowerSupplyChannel.status["IsOn"] == "Unkown":
                self.ui.lvCurrent.setStyleSheet("color: black")
                self.ui.lvVoltage.setStyleSheet("color: red")

        if self.slot.hvPowerSupplyChannel is not None:
            self.ui.hvVoltage.setText( "{:.2f}".format( float ( self.slot.hvPowerSupplyChannel.status["Voltage"] ) ) + "V" )
            if self.slot.hvPowerSupply.config["Model"] != "CAEN":
                self.ui.hvCurrent.setText( "{:.2f}".format( float ( self.slot.hvPowerSupplyChannel.status["Current"] )*10**9 ) + "nA" )
            else:
                self.ui.hvCurrent.setText( "{:.2f}".format( float ( self.slot.hvPowerSupplyChannel.status["Current"] )*10**3 ) + "nA" )
            if self.slot.hvPowerSupplyChannel.status["IsOn"] == "1":
                self.ui.hvCurrent.setStyleSheet("color: green")
                self.ui.hvVoltage.setStyleSheet("color: green")
            if self.slot.hvPowerSupplyChannel.status["IsOn"] == "0":
                self.ui.hvCurrent.setStyleSheet("color: red")
                self.ui.hvVoltage.setStyleSheet("color: red")
            if self.slot.hvPowerSupplyChannel.status["IsOn"] == "Unkown":
                self.ui.hvCurrent.setStyleSheet("color: black")
                self.ui.hvVoltage.setStyleSheet("color: black")

        if self.slot.arduino is not None:
            self.ui.rh.setText( "{:.2f}".format( float ( self.slot.arduino.rh ) ) + "%" )
            self.ui.t.setText( "{:.2f}".format( float ( self.slot.arduino.temp  ) )+ "°C" )