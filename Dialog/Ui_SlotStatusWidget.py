# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SlotStatusWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SlotStatusWidget(object):
    def setupUi(self, SlotStatusWidget):
        if not SlotStatusWidget.objectName():
            SlotStatusWidget.setObjectName(u"SlotStatusWidget")
        SlotStatusWidget.resize(1071, 46)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(SlotStatusWidget.sizePolicy().hasHeightForWidth())
        SlotStatusWidget.setSizePolicy(sizePolicy)
        SlotStatusWidget.setMinimumSize(QSize(0, 40))
        self.horizontalLayoutWidget = QWidget(SlotStatusWidget)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 0, 1051, 39))
        self.slotStatusLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.slotStatusLayout.setObjectName(u"slotStatusLayout")
        self.slotStatusLayout.setContentsMargins(0, 0, 0, 0)
        self.slotCheckBox = QCheckBox(self.horizontalLayoutWidget)
        self.slotCheckBox.setObjectName(u"slotCheckBox")
        self.slotCheckBox.setLayoutDirection(Qt.RightToLeft)

        self.slotStatusLayout.addWidget(self.slotCheckBox)

        self.moduleTypeComboBox = QComboBox(self.horizontalLayoutWidget)
        self.moduleTypeComboBox.addItem("")
        self.moduleTypeComboBox.addItem("")
        self.moduleTypeComboBox.addItem("")
        self.moduleTypeComboBox.setObjectName(u"moduleTypeComboBox")

        self.slotStatusLayout.addWidget(self.moduleTypeComboBox)

        self.moduleIdLineEdit = QLineEdit(self.horizontalLayoutWidget)
        self.moduleIdLineEdit.setObjectName(u"moduleIdLineEdit")
        self.moduleIdLineEdit.setEnabled(True)
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.moduleIdLineEdit.sizePolicy().hasHeightForWidth())
        self.moduleIdLineEdit.setSizePolicy(sizePolicy1)
        self.moduleIdLineEdit.setMinimumSize(QSize(160, 0))

        self.slotStatusLayout.addWidget(self.moduleIdLineEdit)

        self.previousIdButton = QPushButton(self.horizontalLayoutWidget)
        self.previousIdButton.setObjectName(u"previousIdButton")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.previousIdButton.sizePolicy().hasHeightForWidth())
        self.previousIdButton.setSizePolicy(sizePolicy2)
        self.previousIdButton.setMinimumSize(QSize(25, 0))
        self.previousIdButton.setMaximumSize(QSize(30, 16777215))
        icon = QIcon()
        icon.addFile(u"../Utils/reload.png", QSize(), QIcon.Normal, QIcon.Off)
        self.previousIdButton.setIcon(icon)
        self.previousIdButton.setIconSize(QSize(20, 20))

        self.slotStatusLayout.addWidget(self.previousIdButton)

        self.infoButton = QPushButton(self.horizontalLayoutWidget)
        self.infoButton.setObjectName(u"infoButton")
        self.infoButton.setMinimumSize(QSize(35, 0))
        self.infoButton.setMaximumSize(QSize(50, 16777215))

        self.slotStatusLayout.addWidget(self.infoButton)

        self.commentLineEdit = QLineEdit(self.horizontalLayoutWidget)
        self.commentLineEdit.setObjectName(u"commentLineEdit")
        self.commentLineEdit.setEnabled(True)
        sizePolicy1.setHeightForWidth(self.commentLineEdit.sizePolicy().hasHeightForWidth())
        self.commentLineEdit.setSizePolicy(sizePolicy1)
        self.commentLineEdit.setMinimumSize(QSize(80, 0))

        self.slotStatusLayout.addWidget(self.commentLineEdit)

        self.encapsulatedCheckBox = QCheckBox(self.horizontalLayoutWidget)
        self.encapsulatedCheckBox.setObjectName(u"encapsulatedCheckBox")
        self.encapsulatedCheckBox.setLayoutDirection(Qt.RightToLeft)

        self.slotStatusLayout.addWidget(self.encapsulatedCheckBox)

        self.skeletonCheckBox = QCheckBox(self.horizontalLayoutWidget)
        self.skeletonCheckBox.setObjectName(u"skeletonCheckBox")
        self.skeletonCheckBox.setLayoutDirection(Qt.RightToLeft)

        self.slotStatusLayout.addWidget(self.skeletonCheckBox)

        self.hvVoltage = QLabel(self.horizontalLayoutWidget)
        self.hvVoltage.setObjectName(u"hvVoltage")

        self.slotStatusLayout.addWidget(self.hvVoltage)

        self.hvCurrent = QLabel(self.horizontalLayoutWidget)
        self.hvCurrent.setObjectName(u"hvCurrent")

        self.slotStatusLayout.addWidget(self.hvCurrent)

        self.lvVoltage = QLabel(self.horizontalLayoutWidget)
        self.lvVoltage.setObjectName(u"lvVoltage")

        self.slotStatusLayout.addWidget(self.lvVoltage)

        self.lvCurrent = QLabel(self.horizontalLayoutWidget)
        self.lvCurrent.setObjectName(u"lvCurrent")

        self.slotStatusLayout.addWidget(self.lvCurrent)

        self.rh = QLabel(self.horizontalLayoutWidget)
        self.rh.setObjectName(u"rh")

        self.slotStatusLayout.addWidget(self.rh)

        self.t = QLabel(self.horizontalLayoutWidget)
        self.t.setObjectName(u"t")

        self.slotStatusLayout.addWidget(self.t)

        self.stretch = QLabel(self.horizontalLayoutWidget)
        self.stretch.setObjectName(u"stretch")

        self.slotStatusLayout.addWidget(self.stretch)


        self.retranslateUi(SlotStatusWidget)

        QMetaObject.connectSlotsByName(SlotStatusWidget)
    # setupUi

    def retranslateUi(self, SlotStatusWidget):
        SlotStatusWidget.setWindowTitle(QCoreApplication.translate("SlotStatusWidget", u"Form", None))
        self.slotCheckBox.setText(QCoreApplication.translate("SlotStatusWidget", u"Slot", None))
        self.moduleTypeComboBox.setItemText(0, QCoreApplication.translate("SlotStatusWidget", u"2S", None))
        self.moduleTypeComboBox.setItemText(1, QCoreApplication.translate("SlotStatusWidget", u"PS_5G", None))
        self.moduleTypeComboBox.setItemText(2, QCoreApplication.translate("SlotStatusWidget", u"PS_10G", None))

        self.moduleIdLineEdit.setText("")
        self.previousIdButton.setText("")
        self.infoButton.setText(QCoreApplication.translate("SlotStatusWidget", u"Info", None))
        self.commentLineEdit.setText("")
        self.encapsulatedCheckBox.setText(QCoreApplication.translate("SlotStatusWidget", u"Encap.", None))
        self.skeletonCheckBox.setText(QCoreApplication.translate("SlotStatusWidget", u"Skeleton", None))
        self.hvVoltage.setText("")
        self.hvCurrent.setText("")
        self.lvVoltage.setText("")
        self.lvCurrent.setText("")
        self.rh.setText("")
        self.t.setText("")
        self.stretch.setText("")
    # retranslateUi

