ARG FROM_IMAGE=gitlab-registry.cern.ch/cms_tk_ph2/docker_exploration/cmstkph2_udaq_c7
FROM $FROM_IMAGE AS base
SHELL ["/bin/bash", "-c"]

# run container as root 
User root


# go to work directory 
WORKDIR /home/cmsTkUser/gipht/
ADD . /home/cmsTkUser/gipht/

RUN ls -lrt

RUN yum -y install libxkbcommon-x11-devel mesa-libGL-devel xcb-util-wm xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm

RUN yum -y install python3
RUN python3 -m pip install -U pip
RUN python3 -m pip install -r requirements.txt
RUN sh ./compileSubModules.sh

#CMD ["./gipht.py"]
#ENTRYPOINT ["python3"]