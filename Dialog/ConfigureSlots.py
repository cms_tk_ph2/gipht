#------------------------------------------------
#+++++++++++++ConfigureSlots.py+++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      23.02.2021
#------------------------------------------------



from Dialog.Ui_ConfigureSlotsDialog import Ui_ConfigureSlotsDialog
from PySide2.QtWidgets import QDialog,QWidget
from PySide2.QtCore import Qt, Signal
from Dialog.Ui_SlotWidget import Ui_SlotWidget
from src.Module import *

class SlotWidget(QWidget):
    def __init__(self):
        super(SlotWidget, self).__init__()
        self.ui = Ui_SlotWidget()
        self.ui.setupUi(self)

class ConfigureSlotsDialog(QDialog):
    sendSlots = Signal ( object )

    def __init__( self, pSlots, pDeviceList):
        super(ConfigureSlotsDialog, self).__init__()

        self.ui = Ui_ConfigureSlotsDialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Configure Slots")

        self.slots = pSlots

        self.powerSupplyList = []
        self.lvChannelList = ["-"]
        self.hvChannelList = ["-"]

        self.arduinoList     = []
        self.arduinoIdList = ["-"]
        self.ogList = ["OG " + str(i) for i in range(8)]


        for device in pDeviceList:
            if device.config["Type"] == "PowerSupply":
                self.powerSupplyList.append(device)
                for channel in device.channels:
                    if channel.config["HVLV"] =="LV":
                        self.lvChannelList.append(device.config["ID"] + "-" + channel.config["ID"])
                    if channel.config["HVLV"] =="HV":
                        self.hvChannelList.append(device.config["ID"] + "-" + channel.config["ID"] )

            elif device.config["Type"] == "Arduino":
                self.arduinoList.append(device)
                self.arduinoIdList.append(device.config["ID"])

 
        self.ui.cancelButton.clicked.connect(self.closeClicked)
        self.ui.saveConfigurationButton.clicked.connect(self.checkAndSendSlots)

        for slot in self.slots:
            self.ui.slotLayout.addWidget( self.getSlotWidget(slot) )
        
        #self.ui.slotLayout.addStretch()
    
    def getSlotWidget( self, pSlot):
        slotWidget = SlotWidget()
        slotWidget.ui.slotLabel.setText("Slot " + str(pSlot.index))
        slotWidget.ui.lvSupplyComboBox.addItems(self.lvChannelList)
        slotWidget.ui.hvSupplyComboBox.addItems(self.hvChannelList)
        slotWidget.ui.arduinoComboBox.addItems(self.arduinoIdList)
        slotWidget.ui.ogComboBox.addItems(self.ogList)

        if pSlot.lvPowerSupply is not None and pSlot.lvPowerSupplyChannel is not None:
            index = slotWidget.ui.lvSupplyComboBox.findText(pSlot.lvPowerSupply.config["ID"] + "-" + pSlot.lvPowerSupplyChannel.config["ID"], Qt.MatchFixedString)
            if index >= 0:
                slotWidget.ui.lvSupplyComboBox.setCurrentIndex(index)
        if pSlot.hvPowerSupply is not None and pSlot.hvPowerSupplyChannel is not None:
            index = slotWidget.ui.hvSupplyComboBox.findText(pSlot.hvPowerSupply.config["ID"] + "-" + pSlot.hvPowerSupplyChannel.config["ID"], Qt.MatchFixedString)
            if index >= 0:
                slotWidget.ui.hvSupplyComboBox.setCurrentIndex(index)
        if pSlot.arduino is not None:
            index = slotWidget.ui.arduinoComboBox.findText(pSlot.arduino.config["ID"])
            if index >= 0:
                slotWidget.ui.arduinoComboBox.setCurrentIndex(index)
        if pSlot.og is not None:
            index = slotWidget.ui.ogComboBox.findText(pSlot.og)
            if index >= 0:
                slotWidget.ui.ogComboBox.setCurrentIndex(index)
            else:
                slotWidget.ui.ogComboBox.setCurrentIndex(pSlot.index)

        return slotWidget

    #Called when cancel button is clicked, closes the dialog
    def closeClicked( self ):
        self.close()


    def getPowerSupplyAndChannel( self, pPowerSupplyId, pChannelId ):
        searchedPowerSupply     = None
        searchedChannel         = None
        for powersupply in self.powerSupplyList:
            if powersupply.config["ID"] == pPowerSupplyId:
                searchedPowerSupply = powersupply
                for channel in powersupply.channels:
                    if channel.config["ID"] == pChannelId:
                        searchedChannel = channel
                        break
                break
        return searchedPowerSupply, searchedChannel

    def getArduino( self, pArduinoId ):
        searchedArduino = None
        for arduino in self.arduinoList:
            if arduino.config["ID"] == pArduinoId:
                searchedArduino = arduino
                break
        return searchedArduino

    #Last method before the module information is handed back to the GuiController. Checks the input
    def checkAndSendSlots( self ):       
        selectedPowerSupplyChannels = []
        #Update the slot list with the information given in the gui
        for index in range(self.ui.slotLayout.count()):
            slotItem = self.ui.slotLayout.itemAt(index).widget()

            ps = slotItem.ui.lvSupplyComboBox.currentText().split("-")[0]
            ch = slotItem.ui.lvSupplyComboBox.currentText().split("-")[1]
            self.slots[index].lvPowerSupply, self.slots[index].lvPowerSupplyChannel = self.getPowerSupplyAndChannel ( ps, ch )

            ps = slotItem.ui.hvSupplyComboBox.currentText().split("-")[0]
            ch = slotItem.ui.hvSupplyComboBox.currentText().split("-")[1]
            self.slots[index].hvPowerSupply, self.slots[index].hvPowerSupplyChannel = self.getPowerSupplyAndChannel ( ps, ch )

            self.slots[index].arduino = self.getArduino ( slotItem.ui.arduinoComboBox.currentText() )
            self.slots[index].og = slotItem.ui.ogComboBox.currentText()

        for slot in self.slots:
            print(slot.getSlotDictionary())
        self.sendSlots.emit(self.slots)
        self.close()
